

		

		

		

		function setPhoto(){
			<?php 
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}
				
				$user = isset($_POST['id_user'])?$_POST['id_user']:" ";
				$lieu = isset($_POST["lieu"])?$_POST["lieu"] : "";
				$date = isset($_POST["date"])?$_POST["date"] : "";
				$emotion = isset($_POST["emotion"])?$_POST["emotion"] : "";
				$heure = isset($_POST["heure"])?$_POST["heure"] : "";
				$message = isset($_POST["message"])?$_POST["message"] : "";
				$error = "";
				
				if($lieu =="") { $error .= "lieu vide <br/>"; }
				if($date =="") { $error .= "date vide <br/>"; }
				if($emotion =="") { $error .= "emotion <br/>"; }
				if($heure =="") { $error .= "heure vide <br/>"; }
				if($message =="") { $error .= "message vide <br/>"; }
				
				if($user==" "){
					if($error =="") {
						$sql4="UPDATE `information` SET `lien` = '0' WHERE `information`.`lien` = '1'";
						$bdd->exec($sql4);	

						$sql="INSERT INTO information(message,lieu,date_debut,date_fin,heure_debut,heure_fin,`emotion`, `caractere`,lien) VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
						$action = $bdd->prepare($sql);

						$action->execute(array(
							'a'=>$message, 
							'b'=>$lieu, 
							'c'=>$date, 
							'd'=>null, 
							'e'=>$heure, 
							'f'=>'0',
							'g'=>$emotion,
							'h'=>null,
							'i'=>'1'
						));		
						
						$sql1="INSERT INTO `publication`(`num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES (:a, :b, :c, :d,:e,:f)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql1);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>null, 
							'd'=>$user, 
							'e'=>$num, 
							'f'=>'0'
						));

						$sql2="INSERT INTO `image`(`lien_image`, `num_information`, `nom`) VALUES (:a,:b,:c)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql2);
						$action->execute(array(
							'a'=>"lien de l'image", 
							'b'=>$num, 
							'c'=>"nom de l'image", 
						));

						$sql3="UPDATE `publication` SET `num_image`=(SELECT `num_image` FROM `image` WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')) WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')";
						$bdd->exec($sql3);
					}
				}
			?>
		}

		function setVideo(){
			<?php 
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}
				$user = isset($_POST['id_user'])?$_POST['id_user']:" ";
				$lieu = isset($_POST["lieu2"])?$_POST["lieu2"] : "";
				$date = isset($_POST["date2"])?$_POST["date2"] : "";
				$emotion = isset($_POST["emotion2"])?$_POST["emotion2"] : "";
				$heure = isset($_POST["heure2"])?$_POST["heure2"] : "";
				$message = isset($_POST["message2"])?$_POST["message2"] : "";
				$error = "";
				
				if($lieu =="") { $error .= "lieu vide <br/>"; }
				if($date =="") { $error .= "date vide <br/>"; }
				if($emotion =="") { $error .= "emotion <br/>"; }
				if($heure =="") { $error .= "heure vide <br/>"; }
				if($message =="") { $error .= "message vide <br/>"; }
				
				if($user==" "){
					if($error =="") {
						$sql4="UPDATE `information` SET `lien` = '0' WHERE `information`.`lien` = '1'";
						$bdd->exec($sql4);	

						$sql="INSERT INTO information(message,lieu,date_debut,date_fin,heure_debut,heure_fin,`emotion`, `caractere`,lien) VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
						$action = $bdd->prepare($sql);

						$action->execute(array(
							'a'=>$message, 
							'b'=>$lieu, 
							'c'=>$date, 
							'd'=>null, 
							'e'=>$heure, 
							'f'=>'0',
							'g'=>$emotion,
							'h'=>null,
							'i'=>'1'
						));		
						
						$sql1="INSERT INTO `publication`(`num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES (:a, :b, :c, :d,:e,:f)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql1);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>null, 
							'd'=>user, 
							'e'=>$num, 
							'f'=>'0'
						));

						$sql2="INSERT INTO `video`(`lien_video`, `num_information`) VALUES (:a,:b)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql2);
						$action->execute(array(
							'a'=>"lien de la video", 
							'b'=>$num
						));

						$sql3="UPDATE `publication` SET `num_video`=(SELECT `num_video` FROM `video` WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')) WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')";
						$bdd->exec($sql3);
					}
				}
			?>
		}