<!DOCTYPE html>
<html>

<head>
	<!-- BOOTSTRAP -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS-->
	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<link rel="stylesheet" href="css/bootstrap.min.css" >-->
		<link rel="stylesheet" href="http:////netdna.bootstrapcdn.com/bootswatch/4.1.1/minty/bootstrap.min.css">
		<title> test dialogue </title>
	</head>

	<body>
		<div class="container-fluid design-haut">
			<!-- <div class="container"> -->
				<!-- Création de 3 articles -->
				<!-- Systeme de classe -->
				<div class="row">
					<!-- Ajout de la division du haut avec nom, prénom, photo de profil et photo de couverture -->
					<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<h1> Matthieu Colin de Verdiere </h1>
					</div>
					<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<img src="Mon site/photo_couverture.jpg" class="img-fluid" alt="Responsive image" class="rounded">
					</div>
				</div>
				<br/><br/><br/>
				<div class="container fond-gris">
					<div class="row row-color">
						<!-- <div class="container"> -->
							<div class="btn-group btn-group-toggle" data-toggle="buttons">
								<label class="btn btn-primary active">
									<input checked="" autocomplete="off" type="checkbox"> Accueil
								</label>
								<label class="btn btn-primary">
									<input autocomplete="off" type="checkbox"> Vous
								</label>
								<label class="btn btn-primary">
									<input autocomplete="off" type="checkbox"> Mon réseau
								</label>
								<label class="btn btn-primary">
									<input autocomplete="off" type="checkbox"> Notifications
								</label>
								<label class="btn btn-primary">
									<input autocomplete="off" type="checkbox"> Emplois
								</label>
								<label class="btn btn-primary">
									<input autocomplete="off" type="checkbox"> Photo
								</label>
								<label class="btn btn-primary">
									<input autocomplete="off" type="checkbox"> Messagerie
								</label>
							</div>
							<br/><br/><br/>
							<!-- </div> -->
						</div>

						<br/><br/><br/>
						<!-- <div class="container-fluid fond-gris"> -->
							<div class="row">
								<div class = "col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<div class="card border-primary mb-3" style="max-width: auto;margin: 10px;text-align:left">
									<legend> Que voulez-vous publier aujourd'hui? </legend>
									<br/><br/><br/>
									<button type="button" class="btn btn-primary">Photos</button>
									<br/><br/><br/>
									<button type="button" class="btn btn-primary">Vidéos</button>
									<button type="button" class="btn btn-primary">Evenements</button>
								</div>
								</div>
								<div class = "col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;text-align:left">
										<legend>Confidentialité</legend>
										<div class="form-group ">
											<div class="custom-control custom-radio">
												<input id="customRadio1" name="customRadio" class="custom-control-input" checked="" type="radio">
												<label class="custom-control-label" for="customRadio1">Ami</label>
											</div>
											<div class="custom-control custom-radio">
												<input id="customRadio2" name="customRadio" class="custom-control-input" type="radio">
												<label class="custom-control-label" for="customRadio2">Moi uniquement </label>
											</div>
											<div class="custom-control custom-radio">
												<input id="customRadio3" name="customRadio" class="custom-control-input" type="radio">
												<label class="custom-control-label" for="customRadio3">tous</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								 <!-- Saut de 6 colonnes -->
								 <div class="col-lg-offset-12 col-lg-10"> 
								<button type="button" class="btn btn-primary">Publier</button>
								</div>
							</div>
							<br/><br/><br/>
							<div class = "row">
								<!-- <div class="modal"> -->
									<div class="modal-dialog" role="document" style="width:1000px;">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Publication 1</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p>Cherche un stage en informatique</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary">j'aime</button>
												<button type="button" class="btn btn-primary">je commente</button>
											</div>
										</div>
									</div>
									<!-- </div> -->
								</div>


								<div class = "row">
									<div class="modal-dialog" role="document" style="width:1000px;">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Publication 2</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p>Accepté chez IBM!!</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary">j'aime</button>
												<button type="button" class="btn btn-primary">je commente</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- </div> -->
							<!-- </div> -->
						</div>

						<!-- <script type = "text/javascript" src= "js/bootstrap.min.js"> -->

							<!-- BOOTSTRAP -->
							<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
							<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
						</body>

						</html>
