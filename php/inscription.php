<?php
 	// CONNEXION A LA BASE DE DONNEES
	$database="ece weaver";
	$db_handle=mysqli_connect('localhost','root','');
	$db_found=mysqli_select_db($db_handle,$database);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style_inscription.css"/>
		<!-- DEBUT LIBRAIRIES BOOTSTAP -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- FIN LIBRAIRIES BOOTSTAP -->
	</head>
	
	<body>
		<!-- CREATION D'UN CONTENEUR -->
		<div class="container-fluid" style = "background-image:url(../image/fond.jpg);">
			<div class="col-lg-offset-2 col-lg-8" style = "background-color :rgba(255, 255, 255, 0.5);">
					
					<fieldset>
						<!-- Ligne avec le titre -->
						<div class="row">
							<div class="titre text-center">
								<!-- AJOUT DU TITRE -->
								<h1><font face="times new roman">ECE Weaver</font> </h1>
							</div>
						</div>
						<!-- Ligne avec le trait de présentation -->
							<div class="row">
								<hr class="colorgraph" style = "color : black;">
							</div>
							<!-- LIGNE AVEC L'IMAGE DE L'UTILISATEUR SANS PROFIL -->
								<div class="row">
									<div class="col-lg-offset-5 col-lg-1">
										<div class="col-lg-12">
											<img src="../image/inconnu.png" class="img" alt="..." style="text-align:center; border:1px solid black;
											padding:5px; border-radius:10px;"/>
										</div>
									</div>
								</div>
						<!-- Ajout d'un espace après la photo -->
							<div class="row" style="height:0.5cm;">
							</div>
							
					<!-- 	DEBUT FORMULAIRE -->
					<form action="inscription.php" method="post">		   
						<!-- Plusieurs lignes avec les champs de saisie -->
							<div class="row">
								<div class="col-lg-offset-3 col-lg-6">
									
										<input type="pseudo" name="pseudo" id="pseudo" class="form-control input-lg" placeholder="Pseudo">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-offset-3 col-lg-6">
										<input type="prenom" name="prenom" id="prenom" class="form-control input-lg" placeholder="Prenom">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-offset-3 col-lg-6">
										<input type="nom" name="nom" id="nom" class="form-control input-lg" placeholder="Nom">
									</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-offset-3 col-lg-6">
										<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email">
									</div>
							</div>
						    <br>
							<!-- BOUTONS S'INSCRIRE ET ANNULER -->
							<div class="row">
								<div class="col-lg-offset-3 col-lg-3">
									<button type="submit" class="btn btn-lg btn-primary btn-block" style="border-style: solid ;border-color:black;"  
									onclick="document.location.href='connexion.php'; "> S'inscrire </button>
								</div>
								<div class="col-lg-3">
									<input type="button" class="btn btn-lg btn-danger btn-block" value="Annuler" style="border-style solid ;border-color : black;" onclick="document.location.href='connexion.php';">
								</div>
							</div>
							<!-- DESIGN DU BAS DE PAGE -->
							<div class="row" style = "background-color :rgba(255, 255, 255, 0); height:12cm;">
							</div>

					</form>
						<!-- FIN DU FORMULAIRE -->
							<!-- Début du script php -->
							<?php
							// Si tous les champs sont initialisés
							if (isset($_POST["pseudo"]) && isset($_POST["prenom"]) && isset($_POST["nom"]) && isset($_POST["email"]))
							{
								 // Si tous les champs sont remplis
								 if (!empty($_POST["pseudo"]) && !empty($_POST["prenom"]) && !empty($_POST["nom"]) && !empty($_POST["email"]))    
								 	{
								    $pseudo = $_POST["pseudo"];
									$prenom = $_POST["prenom"];
									$nom = $_POST["nom"];
									$email = $_POST["email"];
									$passage = 1;
									//ON INSERE LES INFORMATIONS DE L'UTILISATEUR DANS LA BASE DE DONNEES
									$sql="INSERT INTO utilisateur(nom, prenom, admin, mail, mdp, pseudo) VALUES('$nom','$prenom','1','$email','salut','$pseudo')";
									$db_handle->query($sql);
									//echo "Bonjour ".$_POST["pseudo"].", tres beau pseudo";
									//echo "Ton vrai prenom est : ".$_POST["prenom"]."";
								}
								//SINON ON DEMANDE A L'UTILISATEUR DE REMPLIR TOUS LES CHAMPS
								else {
									echo "Tu dois remplir tous les champs";
								}
							}
							else { 
								$erreur = 1;
							}
							?>
							<!-- Fin du script php -->
						</fieldset>
				<!-- FERMETURE DES DIFFERENTES BALISES -->
				</div>
		</div>
	</body>
</html>