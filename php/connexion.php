<?php
 
	$database="ece weaver";
	$db_handle=mysqli_connect('localhost','root','');
	$db_found=mysqli_select_db($db_handle,$database);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style_inscription.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	
	<body>
		<div class="container-fluid" style = "background-image:url(../image/fond.jpg);">
			<div class="col-lg-offset-2 col-lg-8" style = "background-color :rgba(255, 255, 255, 0.5);">
					

					<fieldset>
						<!-- Ligne avec le titre -->
						<div class="row">
							<div class="titre text-center">
								<h1><font face="times new roman">ECE Weaver</font> </h1>
							</div>
						</div>
						<!-- Ligne avec le trait de présentation -->
							<div class="row">
								<hr class="colorgraph" style = "color : black;">
							</div>
								<div class="row">
									<div class="col-lg-offset-5 col-lg-1">
										<div class="col-lg-12">
											<img src="../image/inconnu.png" class="img" alt="..." style="text-align:center; border:1px solid black;
											padding:5px; border-radius:10px;"/>
										</div>
									</div>
								</div>
						<!-- Ajout d'un espace après la photo -->
							<div class="row" style="height:0.5cm;">
							</div>
							
					<!-- 	DEBUT FORMULAIRE -->
					<form action="connexion.php" method="post">		   
						<!-- Plusieurs lignes avec les champs de saisie -->
							<div class="row">
								<div class="col-lg-offset-3 col-lg-6">
									
										<input type="pseudo" name="pseudo" id="pseudo" class="form-control input-lg" placeholder="Pseudo">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-offset-3 col-lg-6">
										<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email">
									</div>
							</div>
						    <br>
							<!-- BOUTONS D'INSCRIPTION ET DE CONNEXION -->
								<div class="row">
									<div class="col-lg-offset-3 col-lg-3">
										<button type="button" class="btn btn-lg btn-primary btn-block" style="border-style: solid ;border-color:black;"
										onclick="document.location.href='inscription.php'; "> S'inscrire </button>
									</div>
									<div class="col-lg-3">
										<input type="submit" class="btn btn-lg btn-danger btn-block" value="Connexion" style="border-style solid ;border-color : black;" onclick="document.location.href='connexion.php';">
									</div>
								</div>
								<!-- BAS DE PAGE -->
								<div class="row" style = "background-color :rgba(255, 255, 255, 0); height:15cm;">
								</div>

					</form>
						<!-- FIN DU FORMULAIRE -->
						
							<!-- Début du script php -->
							<?php
								// Si tous les champs sont initialisés
								if (isset($_POST["pseudo"]) && isset($_POST["email"]))
								{
									 // Si tous les champs sont remplis
									 if (!empty($_POST["pseudo"]) && !empty($_POST["email"]))    
									 	{
											    $pseudo = $_POST["pseudo"];
												$email = $_POST["email"];
												$passage = 1;
												// Constitution de la requête
												$sql = "SELECT mail,pseudo FROM utilisateur WHERE mail='$email' AND pseudo='$pseudo'";
											 	
											 	$result = mysqli_query($db_handle, $sql); //Fctn qui permet d'exécuter la requête
												//$result = $db_handle->query($sql);
												//@mysql_num_rows
												

											
											    	//L'UTILISATEUR A BIEN REUSSI A SE CONNECTER
											    if (mysqli_num_rows($result)>0)
											    {
											    	echo "<br> connexion reussie";
													$sql2 = "SELECT id_user FROM utilisateur WHERE mail='$email' AND pseudo='$pseudo'";
													$result2 = mysqli_query($db_handle, $sql2);
													while($data = mysqli_fetch_assoc($result2)) {
														$id_user = $data['id_user'];
											    	//On part sur la page du profil
													}
											    	header('location:index.php? user='.$id_user);
											    }
														
												//SINON LA CONNEXION ECHOUE
												else{
													// On compte le nombre de resultats 
													//echo $sql; AFFICHAGE DE LA REQUETE
													echo "<br> utilisateur inexistant"; // A AFFICHER EN POPUP
												     //On reste sur la page courante
											    
												}

												while($row = mysqli_fetch_assoc($result)) //Cette fonction recupere ligne par ligne, puis on affiche les differ colonnes par $data['ID']...
												{
													//echo "mail:".$row['mail'].'<br>';
												}
												// if($row = $result->fetch()) 
												// {
												// 	echo "vous pouvez vous connecter";
												// 	header('location:index.php');
												// } 
												//  else {
												// 	echo "Cet utilisateur n existe pas ";
												//  }
										}
									//SI L'UTILISATEUR N'A PAS REMPLI TOUS LES CHAMPS
									else {
										echo "remplir tous les champs";
										}
								}
						   else { 
								$erreur = 1;
								}
							?>
					<!-- Fin du script php -->
					</fieldset>
				</div>
		</div>
	</body>
</html>