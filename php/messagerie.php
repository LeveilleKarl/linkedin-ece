<!DOCTYPE html>
<html lang="en">

<head>
	<!-- BOOTSTRAP -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS-->
	<link rel="stylesheet" href="http:////netdna.bootstrapcdn.com/bootswatch/4.1.1/minty/bootstrap.min.css">

	<title> Messagerie </title>
	<link rel="stylesheet" type="text/css" href="../css/Messagerie.css">
</head>

<body>
	<?php 
		//CONNEXION A LA BDD
		try{
	 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
		}
		catch(Exception $e){
			die('Erreur : ' . $e->getMessage());
		}
		//RECUPERATION DE L'ID UTILISATEUR
		$user = isset($_POST['id_user'])?$_POST['id_user']:" ";

		if($user==" "){
			?> <meta http-equiv="refresh" content="0; URL=connexion.php" /> <?php
		}
		else{
			/*?> OK : <div> <?php echo $user ?> </div>  <?php*/
		}
	?>

	<div class="container"> 
		<div class="card border-primary mb-3" style="min-height: 700px;max-width: 100%;margin: 10px;padding: 25px;">
			<div class="container-fluid design-haut">
				<!--Début de la partie supérieure-->
				<div class="up">
					<div class="row" style="height:3 cm;">
						<!-- Ajout de la division du haut avec nom, prénom, photo de profil et photo de couverture -->
						<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<br><br><br><br><br><br>
							<h1> 
								<font color="white"> 
									<?php //AFFICHAGE NOM PRENOM
										$sql="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
										$reponse=$bdd->query($sql);
										while($donnees=$reponse->fetch()){
											echo $donnees['nom'];
											?> <br> <?php
											echo $donnees['prenom'];
										}
									?>
								</font> 
							</h1>
						</div>
						<div class = "col-lg-offset-1 col-xs-12 col-sm-12 col-md-6 col-lg-5">
						<!-- <div class="row"> -->
							<?php //AFFICHAGE PHOTO DE PROFIL, PHOTO DE COUVERTURE
								
								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> <img src="<?php echo $var['lien_image']; ?>" id="couverture" width="200" height="200" class="img-fluid" alt="Responsive image" class="rounded" style="border:4px solid white;"> <?php
								}

								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='fond')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> 
										<script type="text/javascript">setBackground("<?php echo $var['lien_image']; ?>");</script>
									<?php
								}

							?>
						<!-- </div> -->
						</div>
					</div>
					<br/>
				</div>
				<!--Fin de la partie supérieure-->

				<!--Début Boutons-->
				<div class="row" style="margin-right: auto;margin-left: auto;">>
					<div
						<div class="center"> <!--BARRE DE NAVIGATION--> 
							<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
								<div class="btn-group mr-2" role="group" aria-label="First group" >
									<form method="post" action="index.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Accueil</button>
									</form>
									<form method="post" action="vous.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Vous</button>
									</form>
									<form method="post" action="mon_reseau.php">
										<button type="submit" name="id_user"" class="btn btn-primary" value="<?php echo $user ?>">Mon réseau</button>
									</form>
									<form method="post" action="notifications.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Notifications</button>
									</form>
									<form method="post" action="emploi.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Emplois</button>
									</form>
									<form method="post" action="album.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Photo</button>
									</form>
									<form method="post" action="messagerie.php">
										<button type="submit" name="id_user" class="btn btn-primary active" value="<?php echo $user ?>">Messagerie</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Fin Boutons-->
				<br/><br/><br/>

				<div class="row content">
					<!-- PARTIE GAUCHE DE L'ECRAN --> 
				    <div class="col-sm-6">
				    	<div class="card border-primary mb-3" style="height: 670px;max-width: 100%;margin: 10px;">
				    		<br>
							<nav style="margin-left: 25px;">
							    <form class="form-inline my-2 my-lg-0">
							      <input class="form-control mr-sm-2" type="text" placeholder="Rechercher"> <!-- BARRE DE RECHERCHE --> 
							      <button class="btn btn-secondary my-2 my-sm-0" type="submit" name="id_user" value="<?php echo $user ?>">Rechercher</button>
							    </form>
							</nav>

							<br>

						    <button type="button" class="btn btn-primary" style="margin-left: auto; margin-right: auto">Nouvelle Conversation</button>

						    <br>
						    <div class="card border-primary mb-3" style="height: 100%;max-width: 90%;margin-left: auto; margin-right: auto;">
						    	<div style="overflow: auto">
								    <div class="list-group" style="margin-left: auto; margin-right: auto">
									  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
									    <div class="d-flex w-100 justify-content-between">
									      <h6 class="mb-1">Sovandara Chhim</h6>
									      <small>1 day ago</small>
									    </div>
									    <p class="mb-1" style="float: left;">Vous : Bonjour !</p>
									  </a>
									  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
									    <div class="d-flex w-100 justify-content-between">
									      <h6 class="mb-1">Matthieu de Verdiere, Sovandara Chhim, Karl Leveille</h6>
									      <small class="text-muted">2 days ago</small>
									    </div>
									    <p class="mb-1" style="float: left;">Vous : Bonjour !</p>
									  </a>
									</div>
								</div>
							</div>
						</div>
				    </div>

				    <div class="col-sm-6">
				    	<div class="card border-primary mb-3" style="height: 530px;max-width: 100%;margin: 10px;">
				    		<div class="form-group">
							  	<fieldset>
								    <label class="control-label">Sovandara Chhim</label>
								    <br>
								    <label class="control-label">1 day ago</label>
								    <div class="card border-primary mb-3" style="height: 443px;max-width: 100%;margin: 10px;">
								    	<div style="overflow: auto">
									    	<div class="card text-white bg-success mb-3" style="max-width: 20rem; min-width: 20rem; ">
											  <div class="card-header">Sovandara</div>
											  <div class="card-body">
											    <p class="card-text">Bonjour !</p>
											  </div>
											</div>
									    		
									    	<div class="card border-primary mb-3" style="max-width: 20rem; min-width: 20rem; float: right">
											  <div class="card-header" >Vous</div>
											  <div class="card-body">
											    <p class="card-text">Bonjour !</p>
											  </div>
											</div>

											
										</div>
								    </div>
							    </fieldset>
							</div>
				    	</div>
				    	<div class="card border-primary mb-3" style="height: 125px;max-width: 100%;margin: 10px;">
		      				<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
		      				<button type="button" class="btn btn-info">Envoyer</button>
				    	</div>	
			    	</div>
		    	</div>
		    </div>
		    <br><br>

		    <footer>
				<small>
					<br>
					Projet Web Dynamique 2018
					<br>
					ECE Paris
					<br>
					Sovandara Chhim, Matthieu Colin de Verdiere, Karl Léveillé
				</small>
			</footer>
	    </div>
	</div>

	
	<!-- BOOTSTRAP -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>

</html>
