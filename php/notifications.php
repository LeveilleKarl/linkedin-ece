<?php 

	$database="ece weaver";
	$db_handle=mysqli_connect('localhost','root','');
	$db_found=mysqli_select_db($db_handle,$database);
	
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<!-- BOOTSTRAP -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS-->
	<link rel="stylesheet" href="http:////netdna.bootstrapcdn.com/bootswatch/4.1.1/minty/bootstrap.min.css">

	<title>Notifications</title>
	<link rel="stylesheet" type="text/css" href="../css/notifications.css">
</head>

<body>
	<?php 
		// Connexion à la bdd grâce au PDO
		try{
	 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
		}
		catch(Exception $e){
			die('Erreur : ' . $e->getMessage());
		}
		// Si nous sommes un utilisateur alors on peut naviguer entre les pages
		$user = isset($_POST['id_user'])?$_POST['id_user']:" ";

		if($user==" "){
			?> <meta http-equiv="refresh" content="0; URL=connexion.php" /> <?php
		}
		else{
			/*?> OK : <div> <?php echo $user ?> </div>  <?php*/
		}
	?>

	<div class="container">
		<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
			<div class="container-fluid design-haut">
				<!--Début de la partie supérieure-->
				<div class="up">
					<div class="row" style="height:3 cm;">
						<!-- Ajout de la division du haut avec nom, prénom, photo de profil et photo de couverture -->
						<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<br><br><br><br><br><br>
							<h1> 
								<font color="white"> 
									<?php
									// On affiche le nom et prénom de l'utilisateur
										$sql="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
										$reponse=$bdd->query($sql);
										while($donnees=$reponse->fetch()){
											echo $donnees['nom'];
											?> <br> <?php
											echo $donnees['prenom'];
										}
									?>
								</font> 
							</h1>
						</div>
						<div class = "col-lg-offset-1 col-xs-12 col-sm-12 col-md-6 col-lg-5">
						<!-- <div class="row"> -->
							<?php
								// Affichage de la photo de profil et de couverture
								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> <img src="<?php echo $var['lien_image']; ?>" id="couverture" width="200" height="200" class="img-fluid" alt="Responsive image" class="rounded" style="border:4px solid white;"> <?php
								}

								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='fond')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> 
										<script type="text/javascript">setBackground("<?php echo $var['lien_image']; ?>");</script>
									<?php
								}

							?>
						<!-- </div> -->
						</div>
					</div>
					<br/>
				</div>
				<!--Fin de la partie supérieure-->

				<!--Début Boutons-->
				<div class="row" style="margin-right: auto;margin-left: auto;">
					<div>
						<div class="center">
							<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
								<div class="btn-group mr-2" role="group" aria-label="First group" >
								<!-- Groupe de boutons pour passer d'une page à une autre -->
									<form method="post" action="index.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Accueil</button>
									</form>
									<form method="post" action="vous.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Vous</button>
									</form>
									<form method="post" action="mon_reseau.php">
										<button type="submit" name="id_user"" class="btn btn-primary" value="<?php echo $user ?>">Mon réseau</button>
									</form>
									<form method="post" action="notifications.php">
										<button type="submit" name="id_user" class="btn btn-primary active" value="<?php echo $user ?>">Notifications</button>
									</form>
									<form method="post" action="emploi.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Emplois</button>
									</form>
									<form method="post" action="album.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Photo</button>
									</form>
									<form method="post" action="messagerie.php">
										<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Messagerie</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Fin Boutons-->
				<br/><br/><br/>
				
				<div class="col-sm-12">
					<br>
			    	<nav style="margin-left: 25px;">
					    <form class="form-inline my-2 my-lg-0">
						<!-- Rechercher une notification -->
							<input class="form-control mr-sm-2" type="text" name="rech_titre" placeholder="Nom notification">
							<button class="btn btn-secondary my-2 my-sm-0" type="submit" name="id_user" value="<?php echo $user ?>">Rechercher</button>
							<?php
								$rech_titre = isset($_POST["rech_titre"])?$_POST["rech_titre"] : "";
								if($rech_titre == "") { $condition = 0 ; }
								if($rech_titre != "") { $condition = 1 ; }
							?>
						
						</form>
					</nav>
					<br>
					
					<?php
						if($db_found)
						{	// on veut obtenir toutes les informations de la table notifications avec ou sans la recherche du titre de la notif
							if($condition == 0) { $requete = "'1'"; }
							if($condition == 1) { $requete = "e.nom LIKE '%$rech_titre%'"; }
							$sql="SELECT e.nom, i.message, i.lieu, i.date_debut, i.heure_debut FROM evenement e, information i WHERE e.num_information = i.num_information AND $requete";
							$result=mysqli_query($db_handle,$sql);
							while($data=mysqli_fetch_assoc($result))
							{ 
								?>
								<div class="card border-primary mb-1" style="width: 100% - 25px;margin: 25px;">
									<div>
										<label class="notif"><?php echo "Titre : ".$data['nom']; ?></label>
										<img src="image.png" class="image" height="150" width="150" alt="Responsive image" />
										<br>
										<label class="notif"><?php echo "Description : ".$data['message']; ?></label>
										<br>
										<label class="notif"><?php echo "Lieu : ".$data['lieu']; ?></label>
										<br>
										<label class="notif"><?php echo "Date / Heure : ".substr($data['date_debut'],0,10). " à " .$data['heure_debut']; ?></label>
									</div>
								</div>
								<br>
							<?php
							}
						}
						mysqli_close($db_handle);
						?>
					<br>
				</div>
			</div>
			<br><br><br><br><br>

		    <footer>
				<small>
					
					<br>
					Projet Web Dynamique 2018
					<br>
					ECE Paris
					<br>
					Sovandara Chhim, Matthieu Colin de Verdiere, Karl Léveillé
				</small>
			</footer>
		</div>
	</div>

	<!-- BOOTSTRAP -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>

</html>