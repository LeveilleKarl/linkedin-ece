<?php 

	$database="ece weaver";
	$db_handle=mysqli_connect('localhost','root','');
	$db_found=mysqli_select_db($db_handle,$database);
	
?>

<!DOCTYPE html>
<html>

	<head>
		<!-- BOOTSTRAP -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="style_inscription.css"/>
		<!-- Bootstrap CSS-->
		<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<link rel="stylesheet" href="css/bootstrap.min.css" >-->
		<link rel="stylesheet" href="http:////netdna.bootstrapcdn.com/bootswatch/4.1.1/minty/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/emploi.css">

		<title>Réseau</title>
		<!-- Permet de cacher ou montrer les pop up -->
		<script type="text/javascript">
			function toggle_visibility(id) {
		        var e = document.getElementById(id);
		        if(e.style.display == 'block')
		           e.style.display = 'none';
		        else
		           e.style.display = 'block';
			}
			<!-- fonction pour ajouter un evenement -->
			function setEvenement(){
			<?php 
				
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}
			
				$nom = isset($_POST["nom"])?$_POST["nom"] : "";
				$poste = isset($_POST["poste"])?$_POST["poste"] : "";
				$date_debut = isset($_POST["date_debut"])?$_POST["date_debut"] : "";
				$date_publi = isset($_POST["date_publi"])?$_POST["date_publi"] : "";
				$duree = isset($_POST["duree"])?$_POST["duree"] : "";
				$description = isset($_POST["description"])?$_POST["description"] : "";
				$adresse_emploi = isset($_POST["adresse_emploi"])?$_POST["adresse_emploi"] : "";
				$tel = isset($_POST["tel"])?$_POST["tel"] : "";
				$email = isset($_POST["email"])?$_POST["email"] : "";
				$error = "";
				
				if($nom =="") { $error .= "nom vide <br/>"; }
				if($poste =="") { $error .= "poste vide <br/>"; }
				if($date_debut =="") { $error .= "date debut vide <br/>"; }
				if($date_publi =="") { $error .= "date publi vide <br/>"; }
				if($duree =="") { $error .= "duree vide <br/>"; }
				if($description =="") { $error .= "description vide <br/>"; }
				if($adresse_emploi =="") { $error .= "adrresse vide <br/>"; }
				if($tel =="") { $error .= "tel vide <br/>"; }
				if($email =="") { $error .= "email  vide <br/>"; }
				if($error =="") {
					$sql="INSERT INTO emplois(id_embaucheur, titre_emploi, description_emploi, mail,
					debut_emploi, duree_emploi, date_publi, num_emploi_tel, nom_entreprise, adresse)
					VALUES('5','$poste','$description','$email','$date_debut','$duree',
					'$date_publi','$tel','$nom','$adresse_emploi')";
					$bdd->exec($sql);
				}
			?>
		}
		</script>
	
	</head>

	<body>

		<?php 
			// On se connecte à la bdd grâce au PDO
			try{
		 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
			}
			catch(Exception $e){
				die('Erreur : ' . $e->getMessage());
			}
			
			$user = isset($_POST['id_user'])?$_POST['id_user']:" ";
			// Si nous sommes un utilisateur alors on peut aller de page en page
			if($user==" "){
				?> <meta http-equiv="refresh" content="0; URL=connexion.php" /> <?php
			}
			else{
				/*?> OK : <div> <?php echo $user ?> </div>  <?php*/
			}
		?>

		<div class="container"> 
			<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
				<div class="container-fluid design-haut">
					<!--Début de la partie supérieure-->
					<div class="up">
					<div class="row" style="height:3 cm;">
						<!-- Ajout de la division du haut avec nom, prénom, photo de profil et photo de couverture -->
						<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<br><br><br><br><br><br>
							<h1> 
								<font color="white"> 
									<?php
										// Affiche le nom et prénom de l'utilisateur
										$sql="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
										$reponse=$bdd->query($sql);
										while($donnees=$reponse->fetch()){
											echo $donnees['nom'];
											?> <br> <?php
											echo $donnees['prenom'];
										}
									?> 
								</font> 
							</h1>
						</div>
						<div class = "col-lg-offset-1 col-xs-12 col-sm-12 col-md-6 col-lg-5">
						<!-- <div class="row"> -->
							<?php
								// Affiche les photos de couverture et de profil
								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> <img src="<?php echo $var['lien_image']; ?>" id="couverture" width="200" height="200" class="img-fluid" alt="Responsive image" class="rounded" style="border:4px solid white;"> <?php
								}

								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='fond')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> 
										<script type="text/javascript">setBackground("<?php echo $var['lien_image']; ?>");</script>
									<?php
								}

							?>
						<!-- </div> -->
						</div>
					</div>
					<br/>
				</div>
					<!--Fin de la partie supérieure-->

					<!--Début Boutons-->
					<div class="row" style="margin-right: auto;margin-left: auto;">
						<div>
							<div class="center">
								<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
									<div class="btn-group mr-2" role="group" aria-label="First group" >
										<!-- Groupe de boutons pour passer de page en page -->
										<form method="post" action="index.php">
											<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Accueil</button>
										</form>
										<form method="post" action="vous.php">
											<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Vous</button>
										</form>
										<form method="post" action="mon_reseau.php">
											<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Mon réseau</button>
										</form>
										<form method="post" action="notifications.php">
											<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Notifications</button>
										</form>
										<form method="post" action="emploi.php">
											<button type="submit" name="id_user" class="btn btn-primary active" value="<?php echo $user ?>>">Emplois</button>
										</form>
										<form method="post" action="album.php">
											<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Photo</button>
										</form>
										<form method="post" action="messagerie.php">
											<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Messagerie</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Fin Boutons-->
					<br/><br/><br/>

					<div class="row content">
						<div class="col-sm-12">
							<div class="search">
								<nav style="margin-left: 7px;">
								    <form class="form-inline my-2 my-lg-0" action="emploi.php" method="post">
									<!-- Permet d'effectuer une recherche par rapport au poste et à la durée de l'emploi -->
								      <input class="form-control mr-sm-2" type="text" name="rech_poste" placeholder="Rechercher par poste">
									  <input class="form-control mr-sm-2" type="text" name="rech_duree" placeholder="Rechercher par durée">
								      <button class="btn btn-secondary my-2 my-sm-0" type="submit" name="id_user" value="<?php echo $user ?>">Rechercher</button>
									  <?php
									    $rech_poste = isset($_POST["rech_poste"])?$_POST["rech_poste"] : "";
									    $rech_duree = isset($_POST["rech_duree"])?$_POST["rech_duree"] : "";
									    
										if(($rech_poste =="")&&($rech_duree =="")) { $condition = 0; }
										if(($rech_poste !="")&&($rech_duree =="")) { $condition = 1; }
										if(($rech_poste =="")&&($rech_duree !="")) { $condition = 2; }
										if(($rech_poste !="")&&($rech_duree !="")) { $condition = 3; }
									    ?>
								    </form>
								</nav>
								<br>
							</div> 
							
							<div class="text-right">
								<button type="button" class="btn btn-primary" style="margin-right:15px" onclick="toggle_visibility('emploi');">Ajouter un emploi</button>
							</div>
							<?php
								// Si on trouve la bdd
								if($db_found)
								{	
									if($condition==0) { $rech_reseau=" '1' "; }	
									if($condition==1) { $rech_reseau=" titre_emploi LIKE '%$rech_poste%' "; }
									if($condition==2) { $rech_reseau=" duree_emploi LIKE '$rech_duree' "; }
									if($condition==3) { $rech_reseau=" titre_emploi LIKE '%$rech_poste%' AND duree_emploi LIKE '$rech_duree' "; }
							
									$sql="SELECT * FROM `emplois` WHERE $rech_reseau";
									$result=mysqli_query($db_handle,$sql);
									// on affiche toutes les offrees d'emploi avec ou sans notre recherche
									while($data=mysqli_fetch_assoc($result))
									{ 
									 	?>
										<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
											<div class="card-header">
												<table>
													<tr>
														<th> <?php echo $data['nom_entreprise']; ?> </th> <!-- ici on affiche l'attribut du nom de l'entreprise -->
													</tr>												  <!-- on fait donc la même chose pour les autres attributs -->
												</table>
											</div>
											<div class="row">
												<div class="col-sm-5" style="padding-top:20px; padding-left:30px;">
													<p class="card-text" > <?php echo "Poste : ".$data['titre_emploi']; ?></p>
												</div>
												<div class="col-sm-4" style="padding-top:20px;">
													<p class="card-text" > <?php echo "Date de début : ".$data['debut_emploi']; ?></p>
												</div>
												<div class="col-sm-3" style="padding-top:20px;">
													<p class="card-text" > <?php echo "Durée : ".$data['duree_emploi']; ?></p>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6" style="padding-top:60px; padding-left:30px;">
													<p class="card-text" ><?php echo "Description : ".$data['description_emploi']; ?></p>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6" style="padding-top:100px; padding-left:30px;">
													<p class="card-text" ><?php echo "Date de publication : ".$data['date_publi']; ?></p>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-5" style="padding-top:60px; padding-left:30px; padding-bottom:50px;">
													<p class="card-text" ><?php echo "Adresse : ".$data['adresse']; ?></p>
												</div>
												<div class="col-sm-4" style="padding-top:60px; padding-bottom:50px;">
													<p class="card-text" ><?php echo "Mail : ".$data['mail']; ?></p>
												</div>
												<div class="col-sm-3" style="padding-top:60px; padding-bottom:50px;">
													<p class="card-text" ><?php echo "Tel : ".$data['num_emploi_tel']; ?></p>
												</div>
											</div>
										</div> 
										<?php
									}
								}
							    mysqli_close($db_handle);
							?>
						</div>
					</div>
				</div>
				<br><br>

			<!-- Pop up pour ajouter un emploi --> 
			<div class="popup" id="emploi">
				<div class="container">
					<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
						<label> <h4> <strong> Ajout d'un emploi </strong> </h4> <img src="image.png" class="image" height="100" width="100" alt="Responsive image" />
						</label>
						
						<form action="emploi.php" method="post">
							<div class="form-group">
								<label> Nom de l'entreprise : </label>
								<input type="text" class="form-control input-lg" id="nom" name="nom">
							</div>
							<br>

							<div class="form-group">
								<label> Poste :</label>
								<input type="text" class="form-control" id="poste" name="poste">
							</div>
							<br>
							
							<div class="row">
								<div class="col-md-6">
									<label> Début Date :</label>
									<input type="date" class="form-control input-lg" id="début" name="date_debut">
								</div>
								<div class="col-md-6">
									<label> Date Publication :</label>
									<input type="date" class="form-control input-lg" id="début" name="date_publi">
								</div>		
							</div>
							<br>
							
							<div class="form-group">
								<label> Durée :</label>
								<input type="text" class="form-control" id="duree" name="duree">
							</div>
							<br>

							<div class="form-group">
								<label> Description :</label>
								<textarea class="form-control" id="description" name="description" rows="3"></textarea>
							</div>
							<br>
							
							<div class="form-group">
								<label> Adresse :</label>
								<input type="text" class="form-control" id="adresse" name="adresse_emploi">
							</div>
							<br>
							
							<div class="row">
								<div class="col-md-6">
									<label> Numéro de téléphone :</label>
									<input type="text" class="form-control input-lg" id="tel" name="tel">
								</div>
								<div class="col-md-6">
									<label> Email :</label>
									<input type="text" class="form-control input-lg" id="email" name="email">
								</div>		
							</div>
							<br>

							<fieldset class="form-group">
								<!-- Si on appuie sur Fermer alors on ferme la pop up -->
							  	<button type="button" class="btn btn-primary" onclick="toggle_visibility('emploi');">Fermer</button>
								<!-- Si on appuie sur Créer alors on ajoute un emploi -->
								<button type="submit" class="btn btn-primary" onclick="setEmploi()" name="id_user" value="<?php echo $user ?>">Créer</button>
							  </div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			
			    <footer>
					<small>
						
						<br>
						Projet Web Dynamique 2018
						<br>
						ECE Paris
						<br>
						Sovandara Chhim, Matthieu Colin de Verdiere, Karl Léveillé
					</small>
				</footer>
			</div>
		</div>
		
		<!-- BOOTSTRAP -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

	</body>

</html>
