<?php 

	$database="ece weaver";
	$db_handle=mysqli_connect('localhost','root','');
	$db_found=mysqli_select_db($db_handle,$database);
	
?>

<!DOCTYPE html>
<html>

	<head>
		<!-- BOOTSTRAP -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="style_inscription.css"/>
		<!-- Bootstrap CSS-->
		<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<link rel="stylesheet" href="css/bootstrap.min.css" >-->
		<link rel="stylesheet" href="http:////netdna.bootstrapcdn.com/bootswatch/4.1.1/minty/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/Mon_r	eseau.css">

		<!-- Titre de la fenetre -->
		<title>Réseau</title>
		
		<!-- Pour cacher ou montrer les popups -->
		<script type="text/javascript">
			function toggle_visibility(id) {
		        var e = document.getElementById(id);
		        if(e.style.display == 'block')
		           e.style.display = 'none';
		        else
		           e.style.display = 'block';
			}
			
			function setSupp_util() {
			<?php
			
				
			
			?>
			}
		</script>
		
	</head>

	<body>
		<?php 
			// Connexion à la base de données avec PDO
			try{
		 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
			}
			catch(Exception $e){
				die('Erreur : ' . $e->getMessage());
			}

			$user = isset($_POST['id_user'])?$_POST['id_user']:" ";

			// Si on a pas d'utilisateur alors on va sur la page d'accueil...
			if($user==" "){
				?> <meta http-equiv="refresh" content="0; URL=connexion.php" /> <?php
			}
			// ... sinon on peut se changer de page
			else{
				/*?> OK : <div> <?php echo $user ?> </div>  <?php*/
			}
		?>
		
		<div class="container"> 
			<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
				<div class="container-fluid design-haut">
					<!--Début de la partie supérieure-->
				<div class="up">
					<div class="row" style="height:3 cm;">
						<!-- Ajout de la division du haut avec nom, prénom, photo de profil et photo de couverture -->
						<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<br><br><br><br><br><br>
							<h1> 
								<font color="white"> 
									<?php
										// On affiche le nom et prénom de l'utilisateur
										$sql="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
										$reponse=$bdd->query($sql);
										while($donnees=$reponse->fetch()){
											echo $donnees['nom'];
											?> <br> <?php
											echo $donnees['prenom'];
										}
									?>
								</font> 
							</h1>
						</div>
						<div class = "col-lg-offset-1 col-xs-12 col-sm-12 col-md-6 col-lg-5">
						<!-- <div class="row"> -->
							<?php
								// On affiche la photo de couverture et la photo de profil
								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> <img src="<?php echo $var['lien_image']; ?>" id="couverture" width="200" height="200" class="img-fluid" alt="Responsive image" class="rounded" style="border:4px solid white;"> <?php
								}

								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='fond')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> 
										<script type="text/javascript">setBackground("<?php echo $var['lien_image']; ?>");</script>
									<?php
								}

							?>
						<!-- </div> -->
						</div>
					</div>
					<br/>
				</div>
				<!--Fin de la partie supérieure-->

				<!--Début Boutons-->
				<div class="row" style="margin-right: auto;margin-left: auto;">
					<div class="center">
						<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
							<div class="btn-group mr-2" role="group" aria-label="First group" >
								<!-- Ce menu permet de changer de page -->
								<form method="post" action="index.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Accueil</button>
								</form>
								<form method="post" action="vous.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Vous</button>
								</form>
								<form method="post" action="mon_reseau.php">
									<button type="submit" name="id_user"" class="btn btn-primary active" value="<?php echo $user ?>">Mon réseau</button>
								</form>
								<form method="post" action="notifications.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Notifications</button>
								</form>
								<form method="post" action="emploi.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Emplois</button>
								</form>
								<form method="post" action="album.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Photo</button>
								</form>
								<form method="post" action="messagerie.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Messagerie</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- Fin Boutons-->
				<br/><br/>
					
					<div class="row content">
						<div class="col-sm-12">
							<div class="search">
								<nav style="margin-left: 5px;">
									<!-- Ce form permet de faire des recherches par rapport aux noms et aux prénoms du réseau -->
								    <form class="form-inline my-2 my-lg-0" action="mon_reseau.php" method="post">
										<input class="form-control mr-sm-2" type="text" name="rech_nom" placeholder="Rechercher par nom">
									    <input class="form-control mr-sm-2" type="text" name="rech_prenom" placeholder="Rechercher par prénom">
								        <button class="btn btn-secondary my-2 my-sm-0" type="submit" name="id_user" value="<?php echo $user ?>">Rechercher</button>
						
									    <?php
									    $rech_nom = isset($_POST["rech_nom"])?$_POST["rech_nom"] : "";
									    $rech_prenom = isset($_POST["rech_prenom"])?$_POST["rech_prenom"] : "";
									    
										if(($rech_nom =="")&&($rech_prenom =="")) { $condition = 0; }
										if(($rech_nom !="")&&($rech_prenom =="")) { $condition = 1; }
										if(($rech_nom =="")&&($rech_prenom !="")) { $condition = 2; }
										if(($rech_nom !="")&&($rech_prenom !="")) { $condition = 3; }
									    ?>
								    </form>
								</nav>
								<br>
							</div> 
							
							<div class="text-right">
								<?php
									try{
										$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
									}
									catch(Exception $e){
										die('Erreur : ' . $e->getMessage());
									}
									// Ici on vérifie si l'utilisateur est un admin...
									$sql = $bdd->query('SELECT admin FROM utilisateur WHERE admin = "2"');
									while($data = $sql->fetch()) {
										$valeur = $data['admin'];
									}
								?> <!-- ... si c'est le cas alors il peut ajouter et supprimer des utilisateurs -->
								<button type="button" class="btn btn-primary" style="margin-right:15px" <?php if($valeur == '2') { ?>onclick="toggle_visibility('ajout');" <?php } ?>>Ajouter un utilisateur</button>
								<button type="button" class="btn btn-primary" style="margin-right:5px" <?php if($valeur == '2') { ?>onclick="toggle_visibility('supp');"<?php } ?>>Supprimer un utilisateur</button>
							</div>
							
							<!--Text "Ami" au centre-->
							<div class="amis" style="text-align:center;">
								<p> Mes amis </p> 
							</div>
						
						<!-- Une ligne pour afficher 2 amis -->
						
						<?php
						if($db_found)
						{	
							$boucle=0;
							if($condition==0) { $rech_reseau=" '1' "; }	
							if($condition==1) { $rech_reseau=" r.nom LIKE '%$rech_nom%' "; }
							if($condition==2) { $rech_reseau=" r.prenom LIKE '%$rech_prenom%' "; }
							if($condition==3) { $rech_reseau=" r.nom LIKE '%$rech_nom%' AND r.prenom LIKE '%$rech_prenom%' "; }
							
							$sql="SELECT r.nom, r.prenom FROM ami a, reseau r WHERE a.id_ami = r.id_ami AND $rech_reseau";
							$result=mysqli_query($db_handle,$sql);
							while($data=mysqli_fetch_assoc($result))
							{ 
								// On affiche tous les amis de l'utilisateur
								if($boucle%2==0) {
								?>
									<div class="row">
										<div class="col-md-6">
											<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
												<div class="card-body">
													<label class="notif"><?php echo $data['nom']; ?></label>
													<label class="notif"><?php echo $data['prenom']; ?></label>
												</div>
											</div>
										</div>
								<?php } 
								else { ?>
										<div class="col-md-6">
											<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
												<div class="card-body">
													<label class="notif"><?php echo $data['nom']; ?></label>
													<label class="notif"><?php echo $data['prenom']; ?></label>
												</div>
											</div>
										</div>
									</div> 
								<?php
								} 	
								$boucle++;
							}
							
						}
						if($boucle%2==1) { ?> </div> <?php }
						?>
						<!--Text "Ami" au centre-->
						 <div class="pro" style="text-align:center;">
						<p> Mes contacts professionnels </p> 
						</div>
						
						
						<!-- Une ligne pour afficher 2 contacts pro -->
						<?php
						if($db_found)
						{	// on veut obtenir toutes les informations de la table emploi
							$boucle2=0;
							if($condition==0) { $rech_reseau=" '1' "; }	
							if($condition==1) { $rech_reseau=" r.nom LIKE '%$rech_nom%' "; }
							if($condition==2) { $rech_reseau=" r.prenom LIKE '%$rech_prenom%' "; }
							if($condition==3) { $rech_reseau=" r.nom LIKE '%$rech_nom%' AND r.prenom LIKE '%$rech_prenom%' "; }
							$sql="SELECT r.nom, r.prenom FROM contact_pro c, reseau r WHERE c.id_contact_pro = r.id_contact_pro AND $rech_reseau";
							$result=mysqli_query($db_handle,$sql);
							while($data=mysqli_fetch_assoc($result))
							{ 
								// Ici on affiche tous les contacts pro de l'utilisateur
								if($boucle%2==0) {
								?>
									<div class="row">
										<div class="col-md-6">
											<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
												<div class="card-body">
													<label class="notif"><?php echo $data['nom']; ?></label>
													<label class="notif"><?php echo $data['prenom']; ?></label>
												</div>
											</div>
										</div>
								<?php } 
								else { ?>
										<div class="col-md-6">
											<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
												<div class="card-body">
													<label class="notif"><?php echo $data['nom']; ?></label>
													<label class="notif"><?php echo $data['prenom']; ?></label>
												</div>
											</div>
										</div>
									</div> 
								<?php
								} 	
								$boucle2++;
							}
						}
						if($boucle2%2==1) { ?> </div> <?php }
						?>
					
							
							<!--Text "Ami" au centre-->
							<div class="tous" style="text-align:center;">
								<p> Tous </p> 
							</div>
						
						<!-- Une ligne pour afficher 2 amis -->
						
						<?php
						if($db_found)
						{	
							$boucle3=0;
							if($condition==0) { $rech_reseau=" '1' "; }	
							if($condition==1) { $rech_reseau=" nom LIKE '%$rech_nom%' "; }
							if($condition==2) { $rech_reseau=" prenom LIKE '%$rech_prenom%' "; }
							if($condition==3) { $rech_reseau=" nom LIKE '%$rech_nom%' AND prenom LIKE '%$rech_prenom%' "; }
							
							$sql="SELECT nom, prenom FROM reseau WHERE $rech_reseau";
							$result=mysqli_query($db_handle,$sql);
							while($data=mysqli_fetch_assoc($result))
							{ 
								// A cet endroit on affiche tous les utilisateurs du réseau
								if($boucle3%2==0) {
								?>
									<div class="row">
										<div class="col-md-6">
											<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
												<div class="card-body">
													<label class="notif"><?php echo $data['nom']; ?></label>
													<label class="notif"><?php echo $data['prenom']; ?></label>
												</div>
											</div>
										</div>
								<?php } 
								else { ?>
										<div class="col-md-6">
											<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
												<div class="card-body">
													<label class="notif"><?php echo $data['nom']; ?></label>
													<label class="notif"><?php echo $data['prenom']; ?></label>
												</div>
											</div>
										</div>
									</div> 
								<?php
								} 	
								$boucle3++;
							}
							
						}
						if($boucle3%2==1) { ?> </div> <?php } ?>
						
					
				<br><br><br>
				
			<!-- Pop up pour que l'admin supprime des utilisateurs -->
			<div class="popup" id="supp">
				<div class="container">
					<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
						<label> <h4> <strong> Supprimer un utilisateur </strong> </h4>
						</label>
						
						<form action="mon_reseau.php" method="post">
							<div class="form-group">
								<label> Nom :  </label>
								<input type="text" class="form-control input-lg" id="nom_supp" name="nom_supp">
							</div>
							<div class="form-group">
								<label> Prenom :</label>
								<input type="text" class="form-control" id="prenom_supp" name="prenom_supp">
							</div>
							<br>
							<?php 
							try{
								$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
								}
								catch(Exception $e){
									die('Erreur : ' . $e->getMessage());
								}
							
								$nom_supp = isset($_POST["nom_supp"])?$_POST["nom_supp"] : "";
								$prenom_supp = isset($_POST["prenom_supp"])?$_POST["prenom_supp"] : "";
								$error = "";
								
								if($nom_supp =="") { $error .= "nom vide <br/>"; }
								if($prenom_supp =="") { $error .= "prenom vide <br/>"; }
								if($error =="") {
									$req = $bdd->query('SELECT * FROM utilisateur');
									while($donnees = $req->fetch()) {
									
									$sql = "DELETE FROM utilisateur WHERE nom LIKE '%$nom_supp%' AND prenom LIKE '%$prenom_supp%' ";
									$bdd->exec($sql);
									}
								}
							?>
							<fieldset class="form-group">
		
							  	<button type="button" class="btn btn-primary" onclick="toggle_visibility('supp');">Fermer</button>
								<button type="submit" class="btn btn-primary" onclick="setSupp_util()" name="id_user" value="<?php echo $user ?>">Créer</button>
							  
							</fieldset>
						</form>
						</div>
					</div>
				</div>
				
			<!-- Pop up pour que l'admin ajoute des utilisateurs -->	
			<div class="popup" id="ajout">
				<div class="container">
					<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
						<label> <h4> <strong> Ajouter un utilisateur </strong> </h4>
						</label>
						
						<form action="mon_reseau.php" method="post">
							<div class="form-group">
								<label> Pseudo :  </label>
								<input type="text" class="form-control input-lg" id="pseudo_ajout" name="pseudo_ajout">
							</div>
							<div class="form-group">
								<label> Prenom :</label>
								<input type="text" class="form-control" id="prenom_ajout" name="prenom_ajout">
							</div>
							<div class="form-group">
								<label> Nom :</label>
								<input type="text" class="form-control" id="nom_ajout" name="nom_ajout">
							</div>
							<div class="form-group">
								<label> Email :</label>
								<input type="text" class="form-control" id="email_ajout" name="email_ajout">
							</div>
							<br>
							<?php 
							try{
								$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
								}
								catch(Exception $e){
									die('Erreur : ' . $e->getMessage());
								}
							
								$pseudo_ajout = isset($_POST["pseudo_ajout"])?$_POST["pseudo_ajout"] : "";
								$prenom_ajout = isset($_POST["prenom_ajout"])?$_POST["prenom_ajout"] : "";
								$nom_ajout = isset($_POST["nom_ajout"])?$_POST["nom_ajout"] : "";
								$email_ajout = isset($_POST["email_ajout"])?$_POST["email_ajout"] : "";

								$error = "";
								
								if($pseudo_ajout =="") { $error .= "pseudo vide <br/>"; }
								if($prenom_ajout =="") { $error .= "prenom vide <br/>"; }
								if($nom_ajout =="") { $error .= "nom vide <br/>"; }
								if($email_ajout =="") { $error .= "email vide <br/>"; }
								if($error =="") {
									
									$sql="INSERT INTO utilisateur(nom, prenom, admin, mail, mdp, pseudo) VALUES('$nom_ajout','$prenom_ajout','1','$email_ajout','salut','$pseudo_ajout')";
									$bdd->exec($sql);
									
								}
							?>
							<fieldset class="form-group">
		
							  	<button type="button" class="btn btn-primary" onclick="toggle_visibility('ajout');">Fermer</button>
								<button type="submit" class="btn btn-primary" onclick="setSupp_util()" name="id_user" value="<?php echo $user ?>">Créer</button>
							  
							</fieldset>
						</form>
						</div>
					</div>
				</div>
				
				
				
			</div>

			    <footer>
					<small>
						<!-- Le footer pour décrire le groupe -->
						
						<br>
						Projet Web Dynamique 2018
						<br>
						ECE Paris
						<br>
						Sovandara Chhim, Matthieu Colin de Verdiere, Karl Léveillé
					</small>
				</footer>

			</div>

		
		</div>

		<!-- BOOTSTRAP -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	</body>

</html>
