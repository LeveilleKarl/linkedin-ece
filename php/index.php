<!DOCTYPE html>
<html>
<!-- DEBUT DU HEAD-->
<head>
	<!-- BOOTSTRAP -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS-->
	<link rel="stylesheet" href="http:////netdna.bootstrapcdn.com/bootswatch/4.1.1/minty/bootstrap.min.css">
	<title> Fil d'actualité </title>
	<link rel="stylesheet" type="text/css" href="../css/accueil.css">

	<!--DEBUT DU JAVASCRIPT-->
	<script type="text/javascript">
		/*Fonction faisant apparaître la popup*/
		function toggle_visibility(id) {
		        var e = document.getElementById(id); //RECHERCHE SELON L'ID DE LA BALISE
		        if(e.style.display == 'block')
		           e.style.display = 'none'; //LA BALISE N'EST PAS AFFICHEE
		        else
		           e.style.display = 'block'; //AFFICHAGE DE LA BALISE
		}

		//////////////////////////////////////////////*POUR PUBLIER DANS LE FIL D'ACTUALITE*/////////////////////////////////////////////////
		//'LIEN' DANS LA TABLE INFORMATION PERMET DE REPERER LA DERNIERE INFORMATION AJOUTEE 
		//PUBLICATION D'UN TEXTE
		function setPublication(){
			<?php
				// CONNEXION A LA BDD
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}

				//RECUPERATION DE L'ID DU USER
				$user = isset($_POST['id_user'])?$_POST['id_user']:" ";

				//ON RECUPERE LE TEXTE DANS TEXTEAREA
				$var = array(
					isset($_POST['publicationtexte'])?$_POST['publicationtexte']:" "
					);

				//S'IL Y A UN UTILISATEUR
				if($user!=" "){
					if($var[0] != " "){
						//AJOUT DES INFORMATIONS DE PUBLICATION
						$sql2="UPDATE `information` SET `lien` = '0' WHERE `information`.`lien` = 1";
						$bdd->exec($sql2);

						$sql="INSERT INTO `information`(`message`, `lieu`, `date_debut`, `date_fin`, `heure_debut`, `heure_fin`, `emotion`, `caractere`, `lien`) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i)";
						$action = $bdd->prepare($sql);
						$action->execute(array(
							'a'=>$var[0], 
							'b'=>null, 
							'c'=>null, 
							'd'=>null, 
							'e'=>'0', 
							'f'=>'0', 
							'g'=>null,
							'h'=>null,
							'i'=>'1'
						));

						//AJOUT DANS PUBLICATION
						$sql1="INSERT INTO `publication`(`num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES (:a, :b, :c, :d,:e,:f)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql1);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>null, 
							'd'=>$user, 
							'e'=>$num, 
							'f'=>'0'
						));

						//AJOUT DE LA POSSIBILITE D'AIMER UN COMMENTAIRE
						$sql3="INSERT INTO cree(`aime`, `num_publication`, `id_user`) VALUES (:a, :b, :c)";
						$reponse = $bdd->query('SELECT `num_publication` FROM `publication` WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE lien="1")');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_publication'];
						}
						$action = $bdd->prepare($sql3);
						$action->execute(array(
							'a'=>'0', 
							'b'=>$num, 
							'c'=>$user
						));
					}
				}
			?>
		}

		/*PUBLICATION D'UN EVENEMENT*/
		function setEvenement(){
			<?php 
				//CONNEXION
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}

				//RECUPERATION DES INFORMATIONS DE L'EVENEMENT
				$user = isset($_POST['id_user'])?$_POST['id_user']:" ";
				$nom = isset($_POST["nom"])?$_POST["nom"] : "";
				$lieu = isset($_POST["lieu"])?$_POST["lieu"] : "";
				$date_debut = isset($_POST["date_debut"])?$_POST["date_debut"] : "";
				$date_fin = isset($_POST["date_fin"])?$_POST["date_fin"] : "";
				$heure_debut = isset($_POST["heure_debut"])?$_POST["heure_debut"] : "";
				$heure_fin = isset($_POST["heure_fin"])?$_POST["heure_fin"] : "";
				$description = isset($_POST["description"])?$_POST["description"] : "";
				$error = "";
				
				//ON VERIFIE SI TOUS LES CHAMPS SONT REMPLIS
				if($nom =="") { $error .= "nom vide <br/>"; }
				if($lieu =="") { $error .= "lieu vide <br/>"; }
				if($date_debut =="") { $error .= "date debut vide <br/>"; }
				if($date_fin =="") { $error .= "date fin vide <br/>"; }
				if($heure_debut =="") { $error .= "heure debut vide <br/>"; }
				if($heure_fin =="") { $error .= "heure fin vide <br/>"; }
				if($description =="") { $error .= "description vide <br/>"; }
				
				//SI UN UTILISATEUR A ETE DEFINI
				if($user!=" "){
					if($error =="") {
						//ON MET LE LIEN A 0  
						$sql4="UPDATE `information` SET `lien` = '0' WHERE `information`.`lien` = '1'";
						$bdd->exec($sql4);	

						$sql="INSERT INTO information(message,lieu,date_debut,date_fin,heure_debut,heure_fin,`emotion`, `caractere`,lien) VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
						$action = $bdd->prepare($sql);

						$action->execute(array(
							'a'=>$description, 
							'b'=>$lieu, 
							'c'=>$date_debut, 
							'd'=>$date_fin, 
							'e'=>$heure_debut, 
							'f'=>$heure_fin,
							'g'=>null,
							'h'=>null,
							'i'=>'1'
						));		
						//AJOUT DANS PUBLICATION
						$sql1="INSERT INTO `publication`(`num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES (:a, :b, :c, :d,:e,:f)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql1);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>null, 
							'd'=>$user, 
							'e'=>$num, 
							'f'=>'0'
						));
						//AJOUT DANS EVENEMENT
						$sql2="INSERT INTO `evenement`(`num_image`, `num_video`, `num_information`, `nom`) VALUES (:a,:b,:c,:d)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql2);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>$num, 
							'd'=>$nom
						));
						//UPDATE DE PUBLICATION AVEC LE NUMERO DE L'EVENEMENT
						$sql3="UPDATE `publication` SET `num_event`=(SELECT `num_event` FROM `evenement` WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')) WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')";
						$bdd->exec($sql3);
					}
				}
			?>
		}

		/*PUBLICATION DE LA PHOTO*/
		function setPhoto(){
			<?php 
				//CONNEXION A LA BDD
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}
				//RECUPERATION DES DONNEES
				$user = isset($_POST['id_user'])?$_POST['id_user']:" ";
				$lieu = isset($_POST["lieu"])?$_POST["lieu"] : "";
				$date = isset($_POST["date"])?$_POST["date"] : "";
				$emotion = isset($_POST["emotion"])?$_POST["emotion"] : "";
				$heure = isset($_POST["heure"])?$_POST["heure"] : "";
				$message = isset($_POST["message"])?$_POST["message"] : "";
				$error = "";
				//ON VERIFIE QUE TOUT EST REMPLI
				if($lieu =="") { $error .= "lieu vide <br/>"; }
				if($date =="") { $error .= "date vide <br/>"; }
				if($emotion =="") { $error .= "emotion <br/>"; }
				if($heure =="") { $error .= "heure vide <br/>"; }
				if($message =="") { $error .= "message vide <br/>"; }
				//SI ON A RECUPERE UN UTILISATEUR
				if($user!=" "){
					if($error =="") {
						//AJOUT DES INFORMATIONS DES PHOTOS
						$sql4="UPDATE `information` SET `lien` = '0' WHERE `information`.`lien` = '1'";
						$bdd->exec($sql4);	

						$sql="INSERT INTO information(message,lieu,date_debut,date_fin,heure_debut,heure_fin,`emotion`, `caractere`,lien) VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
						$action = $bdd->prepare($sql);

						$action->execute(array(
							'a'=>$message, 
							'b'=>$lieu, 
							'c'=>$date, 
							'd'=>null, 
							'e'=>$heure, 
							'f'=>'0',
							'g'=>$emotion,
							'h'=>null,
							'i'=>'1'
						));		
						
						$sql1="INSERT INTO `publication`(`num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES (:a, :b, :c, :d,:e,:f)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql1);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>null, 
							'd'=>$user, 
							'e'=>$num, 
							'f'=>'0'
						));

						$sql2="INSERT INTO `image`(`lien_image`, `num_information`, `nom`) VALUES (:a,:b,:c)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql2);
						$action->execute(array(
							'a'=>"lien de l'image", 
							'b'=>$num, 
							'c'=>"nom de l'image", 
						));

						$sql3="UPDATE `publication` SET `num_image`=(SELECT `num_image` FROM `image` WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')) WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')";
						$bdd->exec($sql3);
					}
				}
			?>
		}
		//PUBLICATION D'UNE VIDEO
		function setVideo(){
			<?php 
				//CONNEXION A LA BDD
				try{
			 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
				}
				catch(Exception $e){
					die('Erreur : ' . $e->getMessage());
				}
				//RECUPERATION DES INFORMATIONS
				$user = isset($_POST['id_user'])?$_POST['id_user']:" ";
				$lieu = isset($_POST["lieu2"])?$_POST["lieu2"] : "";
				$date = isset($_POST["date2"])?$_POST["date2"] : "";
				$emotion = isset($_POST["emotion2"])?$_POST["emotion2"] : "";
				$heure = isset($_POST["heure2"])?$_POST["heure2"] : "";
				$message = isset($_POST["message2"])?$_POST["message2"] : "";
				$error = "";
				//ON REGARDE SI LES CHAMPS SONT REMPLIS
				if($lieu =="") { $error .= "lieu vide <br/>"; }
				if($date =="") { $error .= "date vide <br/>"; }
				if($emotion =="") { $error .= "emotion <br/>"; }
				if($heure =="") { $error .= "heure vide <br/>"; }
				if($message =="") { $error .= "message vide <br/>"; }
				//SI ON A RECUPERE L'ID DE L'UTILISATEUR
				if($user!=" "){
					if($error =="") {
						//MISE A JOUR DE LA BDD AVEC LES INFORMATIONS DE LA VIDEO
						$sql4="UPDATE `information` SET `lien` = '0' WHERE `information`.`lien` = '1'";
						$bdd->exec($sql4);	

						$sql="INSERT INTO information(message,lieu,date_debut,date_fin,heure_debut,heure_fin,`emotion`, `caractere`,lien) VALUES(:a, :b, :c, :d, :e, :f, :g, :h, :i)";
						$action = $bdd->prepare($sql);

						$action->execute(array(
							'a'=>$message, 
							'b'=>$lieu, 
							'c'=>$date, 
							'd'=>null, 
							'e'=>$heure, 
							'f'=>'0',
							'g'=>$emotion,
							'h'=>null,
							'i'=>'1'
						));		
						
						$sql1="INSERT INTO `publication`(`num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES (:a, :b, :c, :d,:e,:f)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql1);
						$action->execute(array(
							'a'=>null, 
							'b'=>null, 
							'c'=>null, 
							'd'=>$user, 
							'e'=>$num, 
							'f'=>'0'
						));

						$sql2="INSERT INTO `video`(`lien_video`, `num_information`) VALUES (:a,:b)";
						$num=0;
						$reponse = $bdd->query('SELECT `num_information` FROM `information` WHERE lien="1"');
						while($donnees = $reponse->fetch()){
							$num=$donnees['num_information'];
						}
						$action = $bdd->prepare($sql2);
						$action->execute(array(
							'a'=>"lien de la video", 
							'b'=>$num
						));

						$sql3="UPDATE `publication` SET `num_video`=(SELECT `num_video` FROM `video` WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')) WHERE `num_information`=(SELECT `num_information` FROM `information` WHERE `lien`='1')";
						$bdd->exec($sql3);
					}
				}
			?>
		}
		////////////////////////FIN DES FONCTIONS POUR PUBLIER//////////////////////////////////

		//BOUTON J'AIME
		function setAime(id){
			//ON RECUPERE L'ID DU BOUTON ET ON CHANGE SA COULEUR
			$verif=0
			if((document.getElementById(id).className=="btn btn-primary")&&($verif==0)){
				document.getElementById(id).className = "btn btn-danger";
				$verif=1;
			}

			if((document.getElementById(id).className=="btn btn-danger")&&($verif==0)){
				document.getElementById(id).className = "btn btn-primary";
				$verif=1;
			}
		}

		//CHANGE LA PHOTO DE COUVERTURE
		function setBackground(url) {
			document.getElementById("c").style.backgroundImage = "url("+url+")";
		}
	</script>
	<!--FIN DU JAVASCRIPT-->
</head>
<!--FIN DU HEAD-->

<body>
	<?php 
	 if(isset($_POST['Emplois']) AND ($_POST['Emplois'] == 'Emplois')) { header("Emplois.php"); }
	?>

	<?php 
		//CONNEXION A LA BDD
		try{
	 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
		}
		catch(Exception $e){
			die('Erreur : ' . $e->getMessage());
		}
		//ON RECUPERE L'IDENTIFIANT DE L'UTILISATEUR
		$user = $_GET['user']; /*isset($_POST['id_user'])?$_POST['id_user']:" ";*/
				
		if($user==" "){ /*Si pas d'utilisateur --> redirection*/
			?> <meta http-equiv="refresh" content="0; URL=index.php" /> <?php
		}
		else{
			/*?> OK : <div> <?php echo $user ?> </div>  <?php*/
		}
	?>

	<!-- CONTIENT LES ELEMENTS DE LA PAGE-->
	<div class="container">
		<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;"> <!-- RECTANGLE CONTENANT LES ELEMENTS -->
			<div class="container-fluid design-haut">
				<!--Début de la partie supérieure-->
				<div class="up" id="c">
					<div class="row" style="height:3 cm;">
						<!-- Ajout de la division du haut avec nom, prénom, photo de profil et photo de couverture -->
						<div class = "col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<br><br><br><br><br><br>
							<h1>
								<font color="white"> 
									<?php  //RECUPERATION ET AFFICHAGE DU NOM/PRENOM DE L'UTILISATEUR
										$sql="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
										$reponse=$bdd->query($sql);
										while($donnees=$reponse->fetch()){
											echo $donnees['nom'];
											?> <br> <?php
											echo $donnees['prenom'];
										}
									?>
								</font> 
							</h1>
							
						</div>
						<div class = "col-lg-offset-1 col-xs-12 col-sm-12 col-md-6 col-lg-5">
						<!-- <div class="row"> -->		
							<?php //AJOUT DE LA PHOTO DE PROFIL ET DE LA PHOTO DE COUVERTURE
								
								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> <img src="<?php echo $var['lien_image']; ?>" id="couverture" width="200" height="200" class="img-fluid" alt="Responsive image" class="rounded" style="border:4px solid white;"> <?php
								}

								$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='fond')";
								$b=$bdd->query($a);
								while($var=$b->fetch()){
									?> 
										<script type="text/javascript">setBackground("<?php echo $var['lien_image']; ?>");</script>
									<?php
								}

							?>
						<!-- </div> -->
						</div>
					</div>
					<br/>
				</div>

				<!--Fin de la partie supérieure-->
				<!--Début Boutons (POUR LA NAVIGATION ENTRE LES PAGES)-->
				<div class="row">
					<!-- <div class="center"> -->
						<div class="col-lg-11">
						<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
							<div class="btn-group mr-2" role="group" aria-label="First group">
								<form method="post" action="index.php">
									<button type="submit" name="id_user" class="btn btn-primary active" value="<?php echo $user ?>">Accueil</button>
								</form>
								<form method="post" action="vous.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Vous</button>
								</form>
								<form method="post" action="mon_reseau.php">
									<button type="submit" name="id_user"" class="btn btn-primary" value="<?php echo $user ?>">Mon réseau</button>
								</form>
								<form method="post" action="notifications.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Notifications</button>
								</form>
								<form method="post" action="emploi.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Emplois</button>
								</form>
								<form method="post" action="album.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Photo</button>
								</form>
								<form method="post" action="messagerie.php">
									<button type="submit" name="id_user" class="btn btn-primary" value="<?php echo $user ?>">Messagerie</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- Fin Boutons-->

				<!-- ZONE EN-DESSOUS DES BOUTONS-->
				<div class="container fond-gris">
					<br/><br/>
					<!-- ZONE DE TEXTE POUR LA PUBLICATION -->
					<div class="card border-primary mb-3" style="max-width: auto;margin: 10px;text-align:left">
						<form action="index.php" method="post">
							<div>
								<textarea class="form-control" name="publicationtexte" id="publicationtexte" rows="5" placeholder="Que voulez-vous publier aujourd'hui?" ></textarea>
							</div>
							
							
							<div class="modal-footer" style="margin-right: 200px; margin-left: auto">
								<button type="button" class="btn btn-primary" onclick="toggle_visibility('popphoto');">Photos</button>
								<button type="button" class="btn btn-primary" onclick="toggle_visibility('popvideo');">Vidéos</button>
								<button type="button" class="btn btn-primary" onclick="toggle_visibility('event');">Evenements</button>

								<div class="card border-primary mb-3" style="width: 200px;max-width: 100%;margin: 10px;text-align:left; padding-left: 10px;">
									<!-- CHOIX DE CONFIDENTIALITE -->
									<legend>Confidentialité</legend>
									<div class="form-group ">
										<div class="custom-control custom-radio">
											<input id="customRadio1" name="customRadio" class="custom-control-input" checked="" type="radio">
											<label class="custom-control-label" for="customRadio1">Ami</label>
										</div>
										<div class="custom-control custom-radio">
											<input id="customRadio2" name="customRadio" class="custom-control-input" type="radio">
											<label class="custom-control-label" for="customRadio2">Moi uniquement </label>
										</div>
										<div class="custom-control custom-radio">
											<input id="customRadio3" name="customRadio" class="custom-control-input" type="radio">
											<label class="custom-control-label" for="customRadio3">tous</label>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-offset-12 col-lg-10"> 
								<!-- PUBLICATION DANS LE FIL D'ACTUALITE -->
								<button type="submit" name="id_user" value="<?php echo $user ?>" class="btn btn-primary" style="float: right" onclick="setPublication()">Publier</button>
							</div>
						</form>	
						<br>
					</div>
					<!---->
			
					<!---->
					<?php
						// Si on trouve la bdd
						try{
					 		$bdd = new PDO('mysql:host=localhost;dbname=ece weaver;charset=utf8','root','');
						}
						catch(Exception $e){
							die('Erreur : ' . $e->getMessage());
						}

						//ON RECHERCHE LES PUBLICATIONS DE L'UTILISATEUR
						$sql="SELECT * FROM `publication` WHERE id_user=$user ORDER BY num_publication DESC";

						$reponse=$bdd->query($sql);

						while($donnees=$reponse->fetch()){ 
							//TEXTE
							if(($donnees['num_image']==null)&&($donnees['num_video']==null)&&($donnees['num_event']==null)){

								$r1=$bdd->query('SELECT * FROM information WHERE num_information=\''.$donnees['num_information'].'\'');

								while($d1=$r1->fetch()){
								 	?>
								 		<div class = "row">
											<div class="modal-dialog" role="document" style="width:100%;">
												<div class="modal-content" >
													<div class="modal-header">
														<?php
															$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
															$b=$bdd->query($a);
															while($var=$b->fetch()){
																?> <img src="<?php echo $var['lien_image']; ?>" width="50" height="50" alt="Responsive image" class="rounded" style="margin: 0px;"> <?php
															}
														?>
														<div style="margin-right: auto; margin-left: auto">
															<h5 class="modal-title">
																<?php
																	$requete="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
																	$retour=$bdd->query($requete);
																	while($identifiant=$retour->fetch()){
																		echo $identifiant['nom']." ";
																		echo $identifiant['prenom'];
																	}
																?>
															</h5>
														</div>
														<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"></a>
													    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
													      <a class="dropdown-item" href="#">Modifier</a>
													      <a class="dropdown-item" href="#">Supprimer</a>
													    </div>
													</div>
													<div class="modal-body">
														<p class="card-text"> <?php echo $d1['message'] ?> </p>
													</div>
													<div  class="modal-body" style="margin-right: auto; margin-left: auto">
														<button type="button" class="btn btn-primary"  name="id_user" value="<?php echo $user ?>" id="news-<?php echo $donnees['num_publication'] ?>" onclick="setAime(id)">J'aime</button>
														<button type="button" class="btn btn-primary">Commenter</button>
														<button type="button" class="btn btn-primary">Partager</button>
													</div>
												</div>
											</div>
										</div>
									<?php
								}
							}
							//IMAGE
							if($donnees['num_image']!=null){

								$r1=$bdd->query('SELECT * FROM information WHERE num_information=\''.$donnees['num_information'].'\'');

								while($d1=$r1->fetch()){
								 	?>
								 		<div class = "row">
											<div class="modal-dialog" role="document" style="width:100%;">
												<div class="modal-content" >
													<div class="modal-header">
														<?php
															$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
															$b=$bdd->query($a);
															while($var=$b->fetch()){
																?> <img src="<?php echo $var['lien_image']; ?>" width="50" height="50" alt="Responsive image" class="rounded" style="margin: 0px;"> <?php
															}
														?>
														<div style="margin-right: auto; margin-left: auto">
															<h5 class="modal-title"> 
																<?php
																	$requete="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
																	$retour=$bdd->query($requete);
																	while($identifiant=$retour->fetch()){
																		echo $identifiant['nom']." ";
																		echo $identifiant['prenom'];
																	}
																?>
															</h5>
														</div>
														<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"></a>
													    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
													      <a class="dropdown-item" href="#">Modifier</a>
													      <a class="dropdown-item" href="#">Supprimer</a>
													    </div>
													</div>
													<div class="modal-body">
														<p class="card-text"> 
															Lieu : <?php echo $d1['lieu'] ?> 
															<br>
															Date : <?php echo substr($d1['date_debut'],0,10) ?> Heure : <?php echo $d1['heure_debut'] ?> 
															<br>
															Emotion : <?php echo $d1['emotion'] ?> 
															<br><br>
															<?php echo $d1['message'] ?> 
															<br><br>
															<?php 
																$r2=$bdd->query('SELECT * FROM image WHERE num_information=\''.$donnees['num_information'].'\'');
																while($d2=$r2->fetch()){
																	echo $d2['nom'];
																}
															?>
															<br>
															<img src="../image/photo_couverture.jpg" width="50" height="50" alt="Responsive image" style="position: relative;margin: 0px;margin-left: 90px; margin-right:auto;float: left">
														</p>
													</div>
													<div  class="modal-body" style="margin-right: auto; margin-left: auto">
														<button type="button" class="btn btn-primary">J'aime</button>
														<button type="button" class="btn btn-primary">Commenter</button>
														<button type="button" class="btn btn-primary">Partager</button>
													</div>
												</div>
											</div>
										</div>
									<?php
								}
							}
							//VIDEO
							if($donnees['num_video']!=null){

								$r1=$bdd->query('SELECT * FROM information WHERE num_information=\''.$donnees['num_information'].'\'');

								while($d1=$r1->fetch()){
								 	?>
								 		<div class = "row">
											<div class="modal-dialog" role="document" style="width:100%;">
												<div class="modal-content" >
													<div class="modal-header">
														<?php
															$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
															$b=$bdd->query($a);
															while($var=$b->fetch()){
																?> <img src="<?php echo $var['lien_image']; ?>" width="50" height="50" alt="Responsive image" class="rounded" style="margin: 0px;"> <?php
															}
														?>
														<div style="margin-right: auto; margin-left: auto">
															<h5 class="modal-title"> 
																<?php
																	$requete="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
																	$retour=$bdd->query($requete);
																	while($identifiant=$retour->fetch()){
																		echo $identifiant['nom']." ";
																		echo $identifiant['prenom'];
																	}
																?>
															</h5>
														</div>
														<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"></a>
													    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
													      <a class="dropdown-item" href="#">Modifier</a>
													      <a class="dropdown-item" href="#">Supprimer</a>
													    </div>
													</div>
													<div class="modal-body">
														<p class="card-text"> 
															Lieu : <?php echo $d1['lieu'] ?> 
															<br>
															Date : <?php echo substr($d1['date_debut'],0,10) ?> Heure : <?php echo $d1['heure_debut'] ?> 
															<br>
															Emotion : <?php echo $d1['emotion'] ?> 
															<br><br>
															<?php echo $d1['message'] ?> 
															<br><br>
															<?php 
																$r2=$bdd->query('SELECT * FROM video WHERE num_information=\''.$donnees['num_information'].'\'');
																while($d2=$r2->fetch()){
																	echo $d2['lien_video'];
																}
															?>
															<br>
															<br>
															<img src="../image/photo_couverture.jpg" width="50" height="50" alt="Responsive image" style="position: relative;margin: 0px;margin-left: 90px; margin-right:auto;float: left">
														</p>
													</div>
													<div  class="modal-body" style="margin-right: auto; margin-left: auto">
														<button type="button" class="btn btn-primary">J'aime</button>
														<button type="button" class="btn btn-primary">Commenter</button>
														<button type="button" class="btn btn-primary">Partager</button>
													</div>
												</div>
											</div>
										</div>
									<?php
								}
							}
							//EVENEMENT
							if($donnees['num_event']!=null){

								$r1=$bdd->query('SELECT * FROM information WHERE num_information=\''.$donnees['num_information'].'\'');

								while($d1=$r1->fetch()){
								 	?>
								 		<div class = "row">
											<div class="modal-dialog" role="document" style="width:100%;">
												<div class="modal-content" >
													<div class="modal-header">
														<?php
															$a="SELECT lien_image FROM image WHERE num_image=(SELECT num_image FROM photo WHERE id_user=$user AND type='profil')";
															$b=$bdd->query($a);
															while($var=$b->fetch()){
																?> <img src="<?php echo $var['lien_image']; ?>" width="50" height="50" alt="Responsive image" class="rounded" style="margin: 0px;"> <?php
															}
														?>
														<div style="margin-right: auto; margin-left: auto">
															<h5 class="modal-title"> 
																<?php
																	$requete="SELECT nom, prenom FROM utilisateur WHERE id_user=$user";
																	$retour=$bdd->query($requete);
																	while($identifiant=$retour->fetch()){
																		echo $identifiant['nom']." ";
																		echo $identifiant['prenom'];
																	}
																?>
															</h5>
														</div>
														<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"></a>
													    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
													      <a class="dropdown-item" href="#">Modifier</a>
													      <a class="dropdown-item" href="#">Supprimer</a>
													    </div>
													</div>
													<div class="modal-body">
														<img src="../image/photo_couverture.jpg" width="50" height="50" alt="Responsive image" style="position: relative;margin: 0px;margin-left: 90px; margin-right:auto;float: left">
														<br>
														<br>
														<p> 

															<br>
															Nom de l'évènement :
															<?php 
																$r2=$bdd->query('SELECT * FROM evenement WHERE num_information=\''.$donnees['num_information'].'\'');
																while($d2=$r2->fetch()){
																	 echo $d2['nom'];
																}
															?>
															<br>
															Lieu : <?php echo $d1['lieu'] ?> 
															<br>
															Date début: <?php echo substr($d1['date_debut'],0,10) ?> Heure : <?php echo $d1['heure_debut'] ?> 
															<br>
															Date fin : <?php echo substr($d1['date_fin'],0,10) ?> Heure : <?php echo $d1['heure_fin'] ?> 
															<br><br>
															<?php echo $d1['message'] ?> 
															<br>
															
														</p>
													</div>
													<div  class="modal-body" style="margin-right: auto; margin-left: auto">
														<button type="button" class="btn btn-primary">J'aime</button>
														<button type="button" class="btn btn-primary">Commenter</button>
														<button type="button" class="btn btn-primary">Partager</button>
													</div>
												</div>
											</div>
										</div>
									<?php
								}
							}
						}

					?>
					<!---->		
				</div>
			</div>
			<br><br><br><br><br>

			<!-- Popup evenement -->
			<div class="popup-evenement" id="event">
				<div class="container">
					<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;padding: 25px;">
						<label> Photo / Video : <img src="image.png" class="image" height="150" width="150" alt="Responsive image" />
						</label>
						<br><br>
						<form action="index.php" method="post">
							<div class="form-group">
								<label> Nom de l'évènement : </label>
								<input type="text" class="form-control input-lg" id="nom" name="nom">
							</div>
							<br>

							<div class="form-group">
								<label> Lieu :</label>
								<input type="text" class="form-control" id="lieu" name="lieu">
							</div>
							<br>
							
							<div class="row">
								<div class="col-md-6">
									<label> Début Date :</label>
									<input type="date" class="form-control input-lg" id="début" name="date_debut">
								</div>
								<div class="col-md-6">
									<label> Début Heure :</label>
									<input type="type" class="form-control input-lg" id="début" name="heure_debut">
								</div>		
							</div>
							<br>

							<div class="row">
								<div class="col-md-6">
									<label> Fin Date :</label>
									<input type="date" class="form-control input-lg" id="fin" name="date_fin">
								</div>
								<div class="col-md-6">
									<label> Fin Heure :</label>
									<input type="type" class="form-control input-lg" id="fin" name="heure_fin">
								</div>		
							</div>
							<br>

							<div class="form-group">
								<label> Description :</label>
								<textarea class="form-control" id="description" name="description" rows="3"></textarea>
							</div>
							<br><br>

							<fieldset class="form-group">
							  <legend>Confidentialité</legend>
							  <div class="form-check">
								<label class="form-check-label">
								  <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
								  Amis
								</label>
							  </div>
							  <div class="form-check">
							  <label class="form-check-label">
								  <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
								  Tous
								</label>
							  </div>
							  <br>
							  <div>
							  	<button type="button" class="btn btn-primary" onclick="toggle_visibility('event');">Fermer</button>
								<button type="submit" name="id_user" value="<?php echo $user ?>" class="btn btn-primary" onclick="setEvenement()">Créer</button>
							  </div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<!-- Fin popup evenement -->

			<!-- Début popup photo -->
			<div class="popup-evenement" id="popphoto">
				<div class="container"> 
					<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
						<form action="index.php" method="post">
							<div class="row content" style="padding-left:20px; padding-top:40px;">
								<div class="col-sm-2">
									<input type="lieu" name="lieu" id="lieu" class="form-control input-lg" placeholder="Lieu">
								</div>
					    		
								<div class="col-sm-2">
									<input type="date" name="date" id="date" class="form-control input-lg" placeholder="Date">
								</div>

								<div class="col-sm-2">
									<input type="heure" name="heure" id="heure" class="form-control input-lg" placeholder="Heure">
								</div>

								<div class="col-sm-2">
									<input type="emotion" name="emotion" id="emotion" class="form-control input-lg" placeholder="Emotion">
								</div>
								
								<div class="col-sm-4">
									<img src="https://pics.alphacoders.com/pictures/view/165975" alt="Lamp" width="32" height="32">
								</div> <br><br><br>
								
								<div class="form-group" style="padding-left:16px;">
									<textarea class="form-control" id="message" name="message" rows="6" cols="90" placeholder="Ecrire un message..."></textarea>
								</div>
							</div> <br>
							
							
							<fieldset class="form-group" style="padding-left:22px;">
								<legend>Confidentialité : </legend>
									<div class="form-check">
										<input class="form-check-input" name="amis" type="checkbox" value="">
											<p> Amis uniquement <p>
										<input class="form-check-input" name="tous" type="checkbox" value="">
											<p> Tous <p>
									</div>
							</fieldset> 
							
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" onclick="toggle_visibility('popphoto');">Fermer</button>
								<button type="submit" name="id_user" value="<?php echo $user ?>" class="btn btn-primary" onclick="setPhoto()">Valider</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- Fin popup Photo -->

			<!-- Début popup Video -->
			<div class="popup-evenement" id="popvideo">
				<div class="container"> 
					<form action="index.php" method="post">
						<div class="card border-primary mb-3" style="max-width: 100%;margin: 10px;">
							<div class="row content" style="padding-left:20px; padding-top:40px;">
								<div class="col-sm-2">
									<input type="lieu" name="lieu2" id="lieu2" class="form-control input-lg" placeholder="Lieu">
								</div>
					    		
								<div class="col-sm-2">
									<input type="date" name="date2" id="date2" class="form-control input-lg" placeholder="Date">
								</div>

								<div class="col-sm-2">
									<input type="heure" name="heure2" id="heure2" class="form-control input-lg" placeholder="Heure">
								</div>

								<div class="col-sm-2">
									<input type="emotion" name="emotion2" id="emotion2" class="form-control input-lg" placeholder="Emotion">
								</div>
								
								<div class="col-sm-4">
									<img src="https://pics.alphacoders.com/pictures/view/165975" alt="Lamp" width="32" height="32">
								</div> <br><br><br>
								
								<div class="form-group" style="padding-left:16px;">
									<textarea class="form-control" id="message2" name="message2" rows="6" cols="90" placeholder="Ecrire un message..."></textarea>
								</div>
							</div> <br>
							
							
							<fieldset class="form-group" style="padding-left:22px;">
								<legend>Confidentialité : </legend>
									<div class="form-check">
										<input class="form-check-input" name="amis2" type="checkbox" value="">
											<p> Amis uniquement <p>
										<input class="form-check-input" name="tous2" type="checkbox" value="">
											<p> Tous <p>
									</div>
							</fieldset> 
							
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" onclick="toggle_visibility('popvideo');">Fermer</button>
								<button type="submit" name="id_user" value="<?php echo $user ?>" class="btn btn-primary" onclick="setVideo();">Valider</button>
							</div>					
						</div>
					</form>
				</div>
			</div>
			<!-- Fin popup Video -->


			<footer>
				<small> <!-- LIEN POUR NAVIGUER ENTRE LES PAGES -->
					<br>
					Projet Web Dynamique 2018
					<br>
					ECE Paris
					<br>
					Sovandara Chhim, Matthieu Colin de Verdiere, Karl Léveillé
				</small>
			</footer>
		</div>
	</div>

	<!-- <script type = "text/javascript" src= "js/bootstrap.min.js"> -->
	<!-- BOOTSTRAP -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>

</html>
