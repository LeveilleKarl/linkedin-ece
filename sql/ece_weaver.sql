-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 05 Mai 2018 à 22:34
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ece weaver`
--

-- --------------------------------------------------------

--
-- Structure de la table `acces`
--

CREATE TABLE `acces` (
  `num_acces` int(11) NOT NULL,
  `acces` char(1) DEFAULT NULL,
  `num_publication` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

CREATE TABLE `album` (
  `num_album` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `num_image` int(11) DEFAULT NULL,
  `num_information` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ami`
--

CREATE TABLE `ami` (
  `id_ami` int(11) NOT NULL,
  `id_reseau` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ami`
--

INSERT INTO `ami` (`id_ami`, `id_reseau`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `num_commentaire` int(11) NOT NULL,
  `num_information` int(11) DEFAULT NULL,
  `num_publication` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contact_pro`
--

CREATE TABLE `contact_pro` (
  `id_contact_pro` int(11) NOT NULL,
  `carte_de_visite` varchar(255) DEFAULT NULL,
  `id_reseau` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contact_pro`
--

INSERT INTO `contact_pro` (`id_contact_pro`, `carte_de_visite`, `id_reseau`) VALUES
(2, 'ECE Paris', 3);

-- --------------------------------------------------------

--
-- Structure de la table `cree`
--

CREATE TABLE `cree` (
  `aime` tinyint(1) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `num_publication` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cree`
--

INSERT INTO `cree` (`aime`, `id_user`, `num_publication`) VALUES
(0, 2, 75),
(0, 2, 76),
(0, 2, 77),
(0, 2, 78),
(0, 1, 79);

-- --------------------------------------------------------

--
-- Structure de la table `emplois`
--

CREATE TABLE `emplois` (
  `id_embaucheur` int(11) NOT NULL,
  `titre_emploi` varchar(25) DEFAULT NULL,
  `description_emploi` text,
  `mail` tinytext,
  `id_user` int(11) DEFAULT NULL,
  `num_image` int(11) DEFAULT NULL,
  `num_information` int(11) DEFAULT NULL,
  `debut_emploi` date DEFAULT NULL,
  `duree_emploi` varchar(20) DEFAULT NULL,
  `date_publi` date DEFAULT NULL,
  `num_emploi_tel` varchar(30) NOT NULL,
  `nom_entreprise` varchar(100) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `emplois`
--

INSERT INTO `emplois` (`id_embaucheur`, `titre_emploi`, `description_emploi`, `mail`, `id_user`, `num_image`, `num_information`, `debut_emploi`, `duree_emploi`, `date_publi`, `num_emploi_tel`, `nom_entreprise`, `adresse`) VALUES
(1, 'Vendeur Darty', 'Vendeur dans le rayon electromenager', 'darty@org.fr', 5, 8, 10, '2018-05-30', '2 mois', '2018-05-01', '06 52 34 56 94', 'Darty ', '18 Rue Salvador 77290 Mitry Mory'),
(3, 'Arbitre Ligue1', 'Recrute un arbitre pour les matchs de Ligue1', 'federation.francaise.football@fr.com', 9, 13, 18, '2018-05-25', '1 an', '2018-05-08', '06 54 23 92 34', 'FFF', 'Stade de France Saint Denis');

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `num_event` int(11) NOT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `num_image` int(11) DEFAULT NULL,
  `num_video` int(11) DEFAULT NULL,
  `num_information` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `evenement`
--

INSERT INTO `evenement` (`num_event`, `nom`, `num_image`, `num_video`, `num_information`) VALUES
(5, 'PHP', NULL, NULL, 9),
(4, 'PHP', NULL, NULL, 8),
(6, 'JAVA', NULL, NULL, 10),
(7, 'Pokemon Go ', NULL, NULL, 11),
(8, 'Exposition chocolat', NULL, NULL, 12),
(9, 'Culture coréenne', NULL, NULL, 13),
(10, 'Tour Eiffel', NULL, NULL, 14),
(11, '20km de Paris', NULL, NULL, 15),
(12, 'Salon etudiant', NULL, NULL, 16),
(13, 'Camping ', NULL, NULL, 17),
(14, 'Succès', NULL, NULL, 41),
(15, 'Jeux Olympiques', NULL, NULL, 44),
(16, 'McDo', NULL, NULL, NULL),
(17, 'Bien Dormir', NULL, NULL, 65);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `num_image` int(11) NOT NULL,
  `lien_image` text,
  `nom` varchar(255) DEFAULT NULL,
  `num_information` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`num_image`, `lien_image`, `nom`, `num_information`) VALUES
(21, '../image/inconnu.png', 'profil', 80),
(18, '../image/3.jpg', 'profil', 81),
(19, '../image/4.jpg', 'fond', 82),
(20, '../image/photo_couverture.jpg', 'profil', 83),
(17, '../image/2.jpg', 'fond', 84),
(16, '../image/1.jpg', 'fond', 85),
(14, '../image/image.png', 'profil', 86),
(15, '../image/fond.jpg', 'fond', 87);

-- --------------------------------------------------------

--
-- Structure de la table `information`
--

CREATE TABLE `information` (
  `num_information` int(11) NOT NULL,
  `message` text,
  `lieu` text,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `heure_debut` varchar(5) NOT NULL,
  `heure_fin` varchar(5) NOT NULL,
  `emotion` varchar(12) DEFAULT NULL,
  `caractere` char(1) DEFAULT NULL,
  `lien` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `information`
--

INSERT INTO `information` (`num_information`, `message`, `lieu`, `date_debut`, `date_fin`, `heure_debut`, `heure_fin`, `emotion`, `caractere`, `lien`) VALUES
(88, 'Projet Web dynamique', NULL, NULL, NULL, '0', '0', NULL, NULL, 1),
(87, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(86, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(85, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(84, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(83, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(82, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(81, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0),
(80, 'photo', 'photo', NULL, NULL, '0', '0', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `messagerie`
--

CREATE TABLE `messagerie` (
  `num_message` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_reseau` int(11) DEFAULT NULL,
  `num_information` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `num_photo` int(11) NOT NULL,
  `type` varchar(12) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `num_image` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`num_photo`, `type`, `id_user`, `num_image`) VALUES
(1, 'CV', 1, 1),
(2, 'profil', 2, 14),
(3, 'fond', 2, 15),
(4, 'fond', 1, 16),
(5, 'profil', 1, 17),
(6, 'fond', 3, 18),
(7, 'profil', 3, 19),
(8, 'fond', 4, 20),
(9, 'profil', 4, 21);

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

CREATE TABLE `publication` (
  `num_publication` int(11) NOT NULL,
  `num_image` int(11) DEFAULT NULL,
  `num_video` int(11) DEFAULT NULL,
  `num_event` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `num_information` int(11) DEFAULT NULL,
  `num_acces` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `publication`
--

INSERT INTO `publication` (`num_publication`, `num_image`, `num_video`, `num_event`, `id_user`, `num_information`, `num_acces`) VALUES
(79, NULL, NULL, NULL, 1, 88, 0),
(62, NULL, NULL, NULL, 1, 61, 0),
(63, NULL, NULL, NULL, 2, 62, 0),
(64, NULL, NULL, NULL, 2, 63, 0),
(65, 11, NULL, NULL, 1, 64, 0),
(66, NULL, NULL, 17, 2, 65, 0),
(67, NULL, NULL, NULL, 2, 66, 0),
(68, 12, NULL, NULL, 2, 67, 0),
(69, 13, NULL, NULL, 2, 68, 0),
(70, NULL, 8, NULL, 2, 69, 0),
(71, NULL, 9, NULL, 2, 70, 0),
(72, NULL, NULL, NULL, 2, 71, 0),
(73, NULL, NULL, NULL, 2, 72, 0),
(74, NULL, NULL, NULL, 2, 73, 0),
(75, NULL, NULL, NULL, 2, 74, 0),
(76, NULL, NULL, NULL, 2, 75, 0),
(77, NULL, NULL, NULL, 2, 78, 0),
(78, NULL, NULL, NULL, 2, 79, 0);

-- --------------------------------------------------------

--
-- Structure de la table `relation`
--

CREATE TABLE `relation` (
  `id_user` int(11) DEFAULT NULL,
  `id_reseau` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `reseau`
--

CREATE TABLE `reseau` (
  `id_reseau` int(11) NOT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_ami` int(11) DEFAULT NULL,
  `id_contact_pro` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reseau`
--

INSERT INTO `reseau` (`id_reseau`, `nom`, `prenom`, `id_user`, `id_ami`, `id_contact_pro`) VALUES
(3, 'Sovan', 'Chhim', 1, NULL, 2),
(4, 'Asakura', 'Yoh', 1, 2, NULL),
(6, 'Roberto', 'Raul', 2, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id_user` int(11) NOT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `mdp` varchar(30) DEFAULT NULL,
  `pseudo` varchar(30) DEFAULT NULL,
  `num_acces` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_user`, `nom`, `prenom`, `admin`, `mail`, `mdp`, `pseudo`, `num_acces`) VALUES
(1, 'Lee', 'Seong Yu', 2, 'lee.seongyu@edu.ece.fr', 'salut', 'Khan', NULL),
(2, 'Chhim', 'Sovandara', 1, 'sovandara.chhim@edu.ece.fr', 'jilav', 'Lokhan', NULL),
(3, 'Colin', 'Matthieu', 1, 'colin.matthieu@gmail.com', 'molvar', 'Mattcdv', NULL),
(4, 'Leveille', 'Karl', 1, 'karl.leveille@gmail.com', 'pinah', 'Karlos', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

CREATE TABLE `video` (
  `num_video` int(11) NOT NULL,
  `lien_video` text,
  `num_information` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `vous`
--

CREATE TABLE `vous` (
  `num_profil` int(11) NOT NULL,
  `lien_cv` text,
  `lien_description` text,
  `description_competence` text,
  `description_formation` text,
  `description_experience` text,
  `id_user` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vous`
--

INSERT INTO `vous` (`num_profil`, `lien_cv`, `lien_description`, `description_competence`, `description_formation`, `description_experience`, `id_user`) VALUES
(3, NULL, 'ECE Paris', 'Infomatique : langages web(javascript, php, css, html...)', 'Formation en sécurité informatique', 'CDD chez Thalès', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acces`
--
ALTER TABLE `acces`
  ADD PRIMARY KEY (`num_acces`),
  ADD KEY `num_publication` (`num_publication`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`num_album`),
  ADD KEY `num_image` (`num_image`),
  ADD KEY `num_information` (`num_information`);

--
-- Index pour la table `ami`
--
ALTER TABLE `ami`
  ADD PRIMARY KEY (`id_ami`),
  ADD KEY `id_reseau` (`id_reseau`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`num_commentaire`),
  ADD KEY `num_information` (`num_information`),
  ADD KEY `num_publication` (`num_publication`);

--
-- Index pour la table `contact_pro`
--
ALTER TABLE `contact_pro`
  ADD PRIMARY KEY (`id_contact_pro`),
  ADD KEY `id_reseau` (`id_reseau`);

--
-- Index pour la table `cree`
--
ALTER TABLE `cree`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `num_publication` (`num_publication`);

--
-- Index pour la table `emplois`
--
ALTER TABLE `emplois`
  ADD PRIMARY KEY (`id_embaucheur`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `num_image` (`num_image`),
  ADD KEY `num_information` (`num_information`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`num_event`),
  ADD KEY `num_image` (`num_image`),
  ADD KEY `num_video` (`num_video`),
  ADD KEY `num_information` (`num_information`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`num_image`),
  ADD KEY `num_information` (`num_information`);

--
-- Index pour la table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`num_information`);

--
-- Index pour la table `messagerie`
--
ALTER TABLE `messagerie`
  ADD PRIMARY KEY (`num_message`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_reseau` (`id_reseau`),
  ADD KEY `num_information` (`num_information`);

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`num_photo`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `num_image` (`num_image`);

--
-- Index pour la table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`num_publication`),
  ADD KEY `num_image` (`num_image`),
  ADD KEY `num_video` (`num_video`),
  ADD KEY `num_event` (`num_event`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `num_information` (`num_information`),
  ADD KEY `num_acces` (`num_acces`);

--
-- Index pour la table `relation`
--
ALTER TABLE `relation`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_reseau` (`id_reseau`);

--
-- Index pour la table `reseau`
--
ALTER TABLE `reseau`
  ADD PRIMARY KEY (`id_reseau`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_ami` (`id_ami`),
  ADD KEY `id_contact_pro` (`id_contact_pro`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `num_acces` (`num_acces`);

--
-- Index pour la table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`num_video`),
  ADD KEY `num_information` (`num_information`);

--
-- Index pour la table `vous`
--
ALTER TABLE `vous`
  ADD PRIMARY KEY (`num_profil`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `num_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `num_image` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `information`
--
ALTER TABLE `information`
  MODIFY `num_information` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT pour la table `publication`
--
ALTER TABLE `publication`
  MODIFY `num_publication` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT pour la table `reseau`
--
ALTER TABLE `reseau`
  MODIFY `id_reseau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `video`
--
ALTER TABLE `video`
  MODIFY `num_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `vous`
--
ALTER TABLE `vous`
  MODIFY `num_profil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
